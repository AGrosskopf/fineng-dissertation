#!/bin/bash

# For each raw CSV file
for rawFile in `ls ./raw/raw_*.csv`
do
    # First line contains meta info about the data
    # EG "AAPL (APPLE INC),105.92,+0.12,"
    metaInfo=`head -n 1 $rawFile`
    
    # Extract export time, and the underlying ticker and spot price at export time
    underlyingTicker=`echo $metaInfo | cut -d\  -f1`
    underlyingSpotPrice=`echo $metaInfo | cut -d, -f2`
    
    # Filter out all options not traded on the CBOE
    cboeOptions=`tail -n +4 $rawFile  | grep "E"` # eg 17 Jan 100.00 (AAPL1720A100-E),
    
    # Extract Put bid and offer prices to determine the market price (as the average)
    marketPrice=`echo "$cboeOptions" | cut -d, -f11,12 | awk -F, '{print ($1 + $2)/2}'`
    
    # Extract index column which contains expiry and strike price
    index=`echo "$cboeOptions" | cut -d, -f8`
    
    # Extract expiry date
    expiryYear=`echo "$index" | cut -d\  -f1 | sed -e 's/^/20/gm'`
    expiryMonth=`echo "$index" | cut -d\  -f2| sed -e 's/$/-/gm'`
    expiryDay=`echo "$index" | grep -oE '[0-9]{4}[A-Z0-9]' | cut -c3-4 | sed -e 's/$/-/gm'`
    expiryTS=`paste <(echo "$expiryDay") <(echo "$expiryMonth") <(echo "$expiryYear") --delimiters '' | sed -e 's/--/-/gm' | date -f - -u '+%s'`
    
    # Derive tau = T - t, t=exportDate
    tau=`echo "$expiryTS" | awk -F, '{ print ($1 - systime())/60/60/24/365}'`
    
    # Get strike price
    strikePrice=`echo "$cboeOptions" | cut -d, -f1 | cut -d\  -f3`
    
    # Output P = market price, tau, K = strike price to 
    echo -e "P,tau,K" > "$underlyingTicker.csv"
    paste <(echo "$marketPrice") \
          <(echo "$tau") \
          <(echo "$strikePrice") --delimiters ',' | grep -v ",-"  >> "$underlyingTicker.csv" 
          
done