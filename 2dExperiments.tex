\section{Two-Dimensional Experiments}
As a starting point for our experimentation we will consider the simplest possible use-case for lookup tables where all parameters but $S$ and $T$ are held constant. To achieve such a setup we can take any suggested subspaces described in \ref{cha:lookup-table} and set 

\begin{align*}
K^{\text{min}} &= K^{\text{max}} = K_c \\
r^{\text{min}} &= r^{\text{max}} = r_c \\
\sigma^{\text{min}} &= \sigma^{\text{max}} = \sigma_c \\
\delta^{\text{min}} &= \delta^{\text{max}} = \delta_c
\end{align*}

\subsection{Results}\label{sec:2dresults}

For the results presented below we only consider the Joubert \& Rogers type subspaces given in \ref{sub-sub-sec:JR-subspace-4d} and \ref{sub-sub-sec:JR-subspace-5d}. In particular we set $K_c = 1$, $r_c = 0.06$, $\sigma_c = 0.4$ and $\delta_c = 0$. The variable dimensions are set as follows 

\begin{align*}
S^{\text{min}} = 0.7 &\,\,\,\, S^{\text{max}} = 1.3\\
T^{\text{min}} = 0.003 &\,\,\,\, T^{\text{max}} = 1
\end{align*}

The choice of $T^{\text{min}}$ is a notable departure from Joubert and Rogers and is intended to assess the utility of the interpolation method for valuing options up to the day prior to expiry.

We perform $F = 3000$ interpolations on options whose parameters were randomly selected from a uniform distribution spanning the same subspace underlying the lookup domain. We choose the $k = 5$ nearest neighbors for both MQ and TPS. In addition we set the shape parameter for MQ RBF to be $\epsilon = 7$.

As a benchmark for accuracy we employ the same Leisen Reimer binomial method with 5000 steps. When the LR Binomial method returns an option value of 0.05 or less, the option is discarded. In addition we report the total time taken by each method. This time includes the determination of the relevant local points to use in the interpolation, as well as the interpolation stage itself.

The four error metrics we report are as follows

\begin{table}[ht!]
\centering
\begin{tabular}[c]{ m{6cm} m{3cm}  }
 \textbf{Name} & \textbf{Definition}\\
 \hline
 Mean Relative Error & $ \frac{1}{F}\sum r_i $ \\
 Root Mean Square Error & $ \sqrt{\frac{1}{F}\sum r_i^2} $ \\
 Maximum Relative Error & $ \max{|r_i|} $ \\
 Maximum Absolute Error & $ \max{|e_i|} $ \\
 \hline
\end{tabular}
\caption{Error Metrics}
\label{tbl:error-metrics}
\end{table}

where $r_i$ and $e_i$ are the absolute and relative errors of the i\textsuperscript{th} interpolation respectively. 

\subsubsection{Cartesian}\label{subsubsec:2dcartesian}
For Cartesian domains we will also compare the accuracy of the RBF interpolated value against number of methods. These methods are the bilinear, nearest neighbour, cubic convolution and cubic spline interpolations. The implementation used is the Matlab \verb|interp2| procedure. We select $k = 5$ nodes from each dimension for these methods, giving a total of 25 nodes.

We populate the $x$ axis (which is $\frac{S}{K}$) with $n_1 = 60$ nodes and the $y$ axis (which is $rT$ or just $T$) with  $n_2 = 30$ nodes, giving a total of 1800 nodes. We use an equidistant node distribution along each axis but without the modifications suggested in \ref{sub-sec:discrete-domain}. Table \ref{tbl:jr2d-cartesian} lists the results of these tests on the 2D domain embedded within the Joubert and Rogers 4D subspace (where the $y$ axis is $rT$).

{
\renewcommand{\arraystretch}{1.2}
\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                      & \textbf{Total Time (S)}           & \textbf{MRE}                    & \textbf{Max A}                    & \textbf{Max R}                    & \textbf{RMSE}                   \\
\textbf{LR Binomial}  & {\color[HTML]{FE0000} 948.001273} &                                 &                                   &                                   &                                 \\
\textbf{RBF MQ}       & 3.996508                          & {\color[HTML]{FE0000} 0.651755} & {\color[HTML]{FE0000} 872.774896} & {\color[HTML]{FE0000} 159.879016} & {\color[HTML]{FE0000} 4.078449} \\
\textbf{RBF TPS}      & {\color[HTML]{32CB00} 2.214639}   & 0.609224                        & 12.926327                         & 1.000000                          & 0.743668                        \\
\textbf{Bi-linear}    & 12.755594                         & {\color[HTML]{32CB00} 0.609169} & 12.913018                         & 1.000000                          & {\color[HTML]{32CB00} 0.743524} \\
\textbf{NN}           & 10.766967                         & {\color[HTML]{000000} 0.609219} & {\color[HTML]{32CB00} 12.890000}  & 1.000000                          & 0.743676                        \\
\textbf{Cubic Conv.}  & 9.160452                          & {\color[HTML]{333333} 0.609367} & 12.965962                         & 1.000242                          & 0.743753                        \\
\textbf{Cubic Spline} & 9.074536                          & 0.609365                        & 12.974063                         & 1.000076                          & 0.743751                       
\end{tabular}
\caption{Interpolation results on a 2D Cartesian domain embedded in a 4D subspace}
\label{tbl:jr2d-cartesian}
\end{table}
}

It is clear from these results that the RBF interpolation methods, especially the TPS method, are much faster than their competitors. However all methods were unable to provide consistently accurate approximations with a few wildly inaccurate values having the most significant impact on the above metrics. The MQ method performed particularly badly due to a few instances of interpolation matrices with a high-condition number. Reducing the shape parameter $\epsilon$ was shown to reduce the occurrence of these near-singular matrices. However this solution has the undesirable effect of decreasing the MRE and RMSE accuracy. Instead of experimenting with the shape parameter $\epsilon$ we proceed to re-execute these tests on the 5D domain $(\frac{S}{K}, T)$ given in \ref{sub-sub-sec:JR-subspace-5d}, with $\epsilon$ and all other test parameters remaining unchanged. Table \ref{tbl:partjr2d-cartesian} lists the results of these tests on the 2D domain embedded within the partial Joubert and Rogers 5D subspace.

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                      & \textbf{Total Time (S)}           & \textbf{MRE}                     & \textbf{Max A}                  & \textbf{Max R}                  & \textbf{RMSE}                   \\
\textbf{LR Binomial}  & {\color[HTML]{FE0000} 986.292671} &                                  &                                 &                                 &                                 \\
\textbf{RBF MQ}       & 4.041367                          & {\color[HTML]{000000} 0.000319}  & {\color[HTML]{000000} 0.550730} & {\color[HTML]{FE0000} 1.012380} & {\color[HTML]{000000} 0.051694} \\
\textbf{RBF TPS}      & {\color[HTML]{32CB00} 2.349662}   & {\color[HTML]{32CB00} -0.000166} & 0.518590                        & 0.994704                        & 0.053307                        \\
\textbf{Bi-linear}    & 13.394057                         & {\color[HTML]{FE0000} -0.002300} & 0.214509                        & 0.184443                        & {\color[HTML]{000000} 0.050249} \\
\textbf{NN}           & 11.861804                         & {\color[HTML]{000000} -0.001326} & {\color[HTML]{FE0000} 0.853885} & 0.999988                        & {\color[HTML]{FE0000} 0.105991} \\
\textbf{Cubic Conv.}  & 10.642705                         & {\color[HTML]{333333} -0.001210} & {\color[HTML]{000000} 0.132465} & 0.104752                        & 0.034885                        \\
\textbf{Cubic Spline} & 9.626248                          & -0.000809                        & {\color[HTML]{32CB00} 0.099643} & {\color[HTML]{32CB00} 0.065923} & {\color[HTML]{32CB00} 0.024902}
\end{tabular}
\caption{Interpolation results on a 2D Cartesian domain embedded in a 5D subspace}
\label{tbl:partjr2d-cartesian}
\end{table}

These results are far more encouraging than the previous set of results. Both RBF methods have the best performance amongst all of the methods evaluated. A large part of this is due to the use of the k-d tree to structure the lookup domain. While the RBF TPS method is the fastest, the cubic spline is the most accurate by most measures. It is possible to increase the accuracy of all these methods by increasing the density of the nodes along each axis, however doing so in the case of RBF may not be optimal. In the next section we explore an optimisation that can only be achieved with meshfree methods such as RBF. 

\subsubsection{Fibonacci Spiral}
We know that around the exercise boundary the pricing function changes rapidly and conversely far away from that boundary it exhibits a small gradient. Therefore it is optimal to place a higher density of nodes around the exercise boundary and to have this density decrease with the distance from the boundary. In an effort to approximate such a placement of nodes we propose to place the points on a Fibonnaci spiral bounded by an ellipse, though the ellipse is invisible in the sense that no points are placed on the ellipse itself. This spiral and its enclosing ellipse are centered in each dimension at the mean of the range of that dimension. Hence the centre is placed at $(1, 0.5015)$. The semi axis lengths in each dimension are set to half of the range along that dimension. These lengths are $0.3$ and $0.4985$ in the $x$ and $y$ directions respectively.

\clearpage

In order to generate such a spiral enclosed by an ellipse with semi-axis lengths $(R_1, R_2)$, centered at $(x_c, y_c)$ with number of points $M$ we place points at $(x_i, y_i)$ for $i = 1, 2, ..., M$ where 
\begin{align}\label{eq:fib-spiral-coords}
	x_i &= x_c + R_1 \sqrt{\frac{i - 0.5}{M - 0.5}}\cos(\frac{4 \pi i}{1 + \sqrt{5}}) \\
	y_i &= y_c + R_2 \sqrt{\frac{i - 0.5}{M - 0.5}}\sin(\frac{4 \pi i}{1 + \sqrt{5}})
\end{align}

While it is possible to achieve good results my setting a high value for $M$ there is a more effective way to obtain a high density around the exercise boundary. To achieve this we generate the points on a spiral as described in \ref{eq:fib-spiral-coords} once, and then create duplicates at various scales by scaling $(R_1, R_2)$ by some scaling factors $s_1, s_2, ..., s_t$. The union of these sets of points is then taken to be the lookup domain. In \ref{fig:fibspiral-partjr} there are $t=17$ duplicate spirals, each $M = 800$ points. The scaling factors are $s_p = 2^{\frac{p}{10}}$ for $p = 1, 2, ... 16$ and $s_{17} = \frac{1}{2}$. As some of the points extended into the lower half-axis where $T<=0$ we treat the option as it has expired and apply the payoff function, effectively truncating these points.

\begin{figure}[htbp]
	\centering
		\includegraphics{images/fibspiral-partjr.png}
	\caption{A lookup domain of 13600 points distributed on Fibonacci spirals}
	\label{fig:fibspiral-partjr}
\end{figure}

We perform the same tests as in \ref{subsubsec:2dcartesian} with the same set of test parameters. Of course we are unable to perform the standard linear, nearest neighbour, cubic convolution and cubic spline interpolation methods on scattered data, and so we omit these methods in these tests. In \ref{tbl:fib-spiral-results-4d} we list the results of these tests on the Fibonacci spiral type domain embedded in the 4D Joubert and Rogers subspace.

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                     & \textbf{Total Time (S)}           & \textbf{MRE}                    & \textbf{Max A}                     & \textbf{Max R}                    & \textbf{RMSE}                   \\
\textbf{LR Binomial} & {\color[HTML]{FE0000} 954.162924} &                                 &                                    &                                   &                                 \\
\textbf{RBF MQ}      & 4.841251                          & {\color[HTML]{FE0000} 0.609710} & {\color[HTML]{32CB00} 13.124788}   & {\color[HTML]{32CB00} 1.000000}   & {\color[HTML]{32CB00} 0.741755} \\
\textbf{RBF TPS}     & {\color[HTML]{32CB00} 2.701166}   & {\color[HTML]{32CB00} 0.448728} & {\color[HTML]{FE0000} 2874.714639} & {\color[HTML]{FE0000} 132.176699} & {\color[HTML]{FE0000} 3.850164}
\end{tabular}
\caption{Interpolation results on a Fibonacci spiral type domain in a 4D subspace}
\label{tbl:fib-spiral-results-4d}
\end{table}

It is fair to conclude that these results are not in the least bit encouraging. Certainly with the absolute errors present in the TPS approximation one would not expect to find any practical application. Nonetheless we press on and repeat the test but this time with a Fibonacci spiral type domain embedded in the 5D partial Joubert and Rogers subspace. These results are given in table \ref{tbl:fib-spiral-results-5d}

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                     & \textbf{Total Time (S)}           & \textbf{MRE}                     & \textbf{Max A}                   & \textbf{Max R}                  & \textbf{RMSE}                   \\
\textbf{LR Binomial} & {\color[HTML]{FE0000} 976.319735} &                                  &                                  &                                 &                                 \\
\textbf{RBF MQ}      & 4.457265                          & {\color[HTML]{32CB00} -0.000112} & {\color[HTML]{32CB00} 0.210484}  & {\color[HTML]{32CB00} 0.164182} & {\color[HTML]{32CB00} 0.005879} \\
\textbf{RBF TPS}     & {\color[HTML]{32CB00} 2.832490}   & {\color[HTML]{FE0000} -0.000537} & {\color[HTML]{FE0000} 14.746259} & {\color[HTML]{FE0000} 3.126897} & {\color[HTML]{FE0000} 0.062524}
\end{tabular}
\caption{Interpolation results on a Fibonacci spiral type domain in a 5D subspace}
\label{tbl:fib-spiral-results-5d}
\end{table}

This set of results is a large improvement on the previos one. The MQ RBF method performs particularly well, and in terms of accuracy is roughly on par with that of the cubic spline listed in table \ref{tbl:partjr2d-cartesian}. It is also worth noting that this is in spite of that fact that the cubic spline method in those previous tests took roughly twice the time of the MQ method in these tests. If speed rather than accuracy is the primary concern than the TPS method shows great promise. However one cannot recommend this method without reservation as there is occasionally a large error in the interpolated value, though these are few and far between. One potential solution is to increase the number of nearest neighbours accounted for in the interpolation, however this will of course have a detrimental affect on the time complexity of the method. 

It is also worth noting that the Matlab \verb|interp2| implementation of the cubic spline method requires at least $k = 4$ nodes in each dimension. Decreasing $k$ from $5$ to $4$ has negligible impact on the time taken, and a small detrimental effect on the accuracy. Thus we can conclude that the cubic spline has hit a lower boundary on the time taken, and this is irrespective of the density or placement of the nodes in the lookup domain.
