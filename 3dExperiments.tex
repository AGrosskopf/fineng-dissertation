\section{Three-Dimensional Experiments}
The next class of tests we will perform will be on a 3-dimensional lookup table where all parameters but $S$, $T$ and $\sigma$ are held constant. As we did previously we can achieve this with the following settings

\begin{align*}
K^{\text{min}} &= K^{\text{max}} = K_c \\
r^{\text{min}} &= r^{\text{max}} = r_c \\
\delta^{\text{min}} &= \delta^{\text{max}} = \delta_c
\end{align*}

\subsection{Results}
As before we only consider the 4D and 5D Joubert \& Rogers type subspaces. We set $K_c = 1$, $r_c = 0.06$ and $\delta_c = 0$. The variable dimensions are set as follows 

\begin{align*}
S^{\text{min}} = 0.7 &\quad S^{\text{max}} = 1.3\\
\sigma^{\text{min}} = 0.03 &\quad \sigma^{\text{max}} = 0.6\\
T^{\text{min}} = 0.003 &\quad T^{\text{max}} = 1
\end{align*}

Again we will perform $F = 3000$ interpolations on options derived from a uniform distribution, and we choose the $k = 5$ nearest neighbors for both MQ and TPS. The shape parameter for MQ RBF remains $\epsilon = 7$ as does the choice of the Leisen Reimer binomial method with 5000 steps as the benchmark for accuracy.

\subsubsection{Cartesian}
We populate the $x$ axis (which is still $\frac{S}{K}$) with $n_1 = 21$ nodes, the $y$ axis (which is either $rT$ or just $T$) with $n_2 = 21$ nodes and the $z$ axis (which is either $\sigma^2T$ or just $\sigma$) with $n_3 = 16$ nodes. We perform the same tests as in the previous section with the same set of methods. The results are listed in table \ref{tbl:jr3d-cartesian}.

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                      & \textbf{Total Time (S)}            & \textbf{MRE}                    & \textbf{Max A}                   & \textbf{Max R}                    & \textbf{RMSE}                   \\
\textbf{LR Binomial}  & {\color[HTML]{FE0000} 1073.299111} &                                 &                                  &                                   &                                 \\
\textbf{RBF MQ}       & 4.5216                             & {\color[HTML]{32CB00} 0.001384} & {\color[HTML]{32CB00} 3.922236}  & {\color[HTML]{000000} 5.411871}   & {\color[HTML]{000000} 0.416572} \\
\textbf{RBF TPS}      & {\color[HTML]{32CB00} 2.687903}    & {\color[HTML]{000000} 0.001493} & 3.922662                         & {\color[HTML]{32CB00} 5.327526}   & {\color[HTML]{34FF34} 0.416021} \\
\textbf{Bi-linear}    & 40.234303                          & {\color[HTML]{000000} NaN}      & 11.482052                        & 143.203387                        & {\color[HTML]{000000} NaN}      \\
\textbf{NN}           & 34.673834                          & {\color[HTML]{000000} NaN}      & {\color[HTML]{000000} 12.035333} & 143.456229                        & {\color[HTML]{000000} NaN}      \\
\textbf{Cubic Conv.}  & 34.498645                          & {\color[HTML]{333333} NaN}      & {\color[HTML]{000000} 11.520442} & {\color[HTML]{FE0000} 143.477208} & NaN                             \\
\textbf{Cubic Spline} & 34.691687                          & -1.605178                       & {\color[HTML]{FE0000} 28.973346} & {\color[HTML]{000000} 143.237828} & {\color[HTML]{000000} 8.120023}
\end{tabular}
\caption{Interpolation results on a 3D Cartesian domain embedded in a 4D subspace}
\label{tbl:jr3d-cartesian}
\end{table}

\begin{remark}[NaN Values]
The first point to make about these results is the presence of NaN values. Further analysis shows that the Matlab \verb|interp3| procedure requires at least 4 local interpolation points in each dimension and at least one of these must be on either side of the point to be interpolated. For methods other than the cubic spline, if the latter condition is not satisfied, IE the point to be interpolated is placed at an edge of the lookup domain, the procedure will return NaN. Since all binary logical relational operators are defined to be false where NaN is concerned, the \verb|max| procedure (used in the computation of the Max R and Max A metrics) effectively ignores these results. This problem can be eliminated by slightly enlarging the lookup domain in each direction, or else recognizing that the target points lies on an edge and performing 2D interpolation instead.
\end{remark}

While all results for all the methods considered are not particularly encouraging due to their potential for high absolute and relative errors, it should be noted that these effects are least pronounced in the RBF-type methods. This is in addition to their overall superior speed. We now consider what effect embedding the same mesh in the 5D partial Joubert \& Rogers subspace has. Table \ref{tbl:partjr3d-cartesian} lists the results of the same set of tests on the 3D Cartesian lookup domain embedded in the 5D partial Joubert and Rogers subspace.

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                      & \textbf{Total Time (S)}            & \textbf{MRE}                     & \textbf{Max A}                    & \textbf{Max R}                    & \textbf{RMSE}                    \\
\textbf{LR Binomial}  & {\color[HTML]{FE0000} 1081.609997} &                                  &                                   &                                   &                                  \\
\textbf{RBF MQ}       & 3.799486                           & {\color[HTML]{32CB00} -0.009371} & {\color[HTML]{32CB00} 0.631064}   & {\color[HTML]{32CB00} 2.438171}   & {\color[HTML]{32CB00} 0.090313}  \\
\textbf{RBF TPS}      & {\color[HTML]{32CB00} 2.306175}    & {\color[HTML]{000000} -0.016608} & 1.900169                          & 6.011285                          & 0.188223                         \\
\textbf{Bi-linear}    & 34.556604                          & {\color[HTML]{000000} NaN}       & 10.682243                         & 22.807063                         & {\color[HTML]{000000} NaN}       \\
\textbf{NN}           & 31.956165                          & {\color[HTML]{000000} NaN}       & {\color[HTML]{000000} 11.311448}  & 24.703388                         & {\color[HTML]{000000} NaN}       \\
\textbf{Cubic Conv.}  & 31.717043                          & {\color[HTML]{333333} NaN}       & {\color[HTML]{000000} 10.700026}  & 22.650420                         & NaN                              \\
\textbf{Cubic Spline} & 31.836066                          & -5.556992                        & {\color[HTML]{FE0000} 737.259141} & {\color[HTML]{FE0000} 499.462845} & {\color[HTML]{000000} 28.994128}
\end{tabular}
\caption{Interpolation results on a 3D Cartesian domain embedded in a 5D subspace}
\label{tbl:partjr3d-cartesian}
\end{table}

The MQ method is best performing of all those considered. By some measures it is more accurate in these tests than the previous ones, most notable in the decreased potential for large absolute errors. The same does not hold true for the TPS method, though it remains the fastest of all the methods considered. All other methods exhibit unacceptable errors.

\subsubsection{Icosahedral}
Another interesting shape on which we can base our domain is the icosahedral. The inspiration for this choice was \cite{Tong2001} who used such a shape in conjunction with the MQ RBF method. An icosahedral is constructed by placing $M$ nodes on a unit sphere such that the total pairwise distance between each node and its neighbours is minimized. 
\clearpage
As with the Fibonacci spiral we set the semi-axis lengths in each dimension according to the range of that dimension. In order to increase the node density around the early-exercise boundary we replicate the icosahedral at various scales. The centre is set to be $(1, 0, 0)$. Negative values $T$ are treated as though the option has expired, and thus the payoff function is applied. We use the shape parameter $\epsilon = 10^{-26}$ take $K = 30$ nearest neighbours.

The results for these tests withing the usual 4D and 5D subspaces are very poor and are included in \ref{tbl:jr3d-ico} and \ref{tbl:partjr3d-ico} for the sake of completeness.

\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                     & \textbf{Total Time (S)}            & \textbf{MRE}               & \textbf{Max A}                   & \textbf{Max R}                   & \textbf{RMSE}              \\
\textbf{LR Binomial} & {\color[HTML]{FE0000} 1043.736378} &                            &                                  &                                  &                            \\
\textbf{RBF MQ}      & 13.117873                          & {\color[HTML]{000000} NaN} & {\color[HTML]{32CB00} 3.140028}  & {\color[HTML]{32CB00} 14.685053} & {\color[HTML]{000000} NaN} \\
\textbf{RBF TPS}     & {\color[HTML]{000000} 12.222112}   & {\color[HTML]{000000} NaN} & {\color[HTML]{FE0000} 32.713679} & {\color[HTML]{FE0000} 19.621284} & {\color[HTML]{000000} NaN}
\end{tabular}
\caption{Interpolation results on an icosahedral-type domain embedded in a 4D subspace}
\label{tbl:jr3d-ico}
\end{table}


\begin{table}[h!]
\centering
\begin{tabular}{llllll}
                     & \textbf{Total Time (S)}           & \textbf{MRE}                     & \textbf{Max A}                  & \textbf{Max R}                   & \textbf{RMSE}                   \\
\textbf{LR Binomial} & {\color[HTML]{FE0000} 949.895569} &                                  &                                 &                                  &                                 \\
\textbf{RBF MQ}      & 11.753523                         & {\color[HTML]{000000} -0.052941} & {\color[HTML]{FE0000} 5.516820} & {\color[HTML]{FE0000} 13.545320} & {\color[HTML]{000000} 0.353260} \\
\textbf{RBF TPS}     & {\color[HTML]{32CB00} 10.043440}  & {\color[HTML]{000000} NaN}       & {\color[HTML]{32CB00} 1.556676} & {\color[HTML]{32CB00} 9.286959}  & {\color[HTML]{000000} NaN}     
\end{tabular}
\caption{Interpolation results on an icosahedral-type domain embedded in a 5D subspace}
\label{tbl:partjr3d-ico}
\end{table}


\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.7]{images/icosahedral-jr3d.png}
	\caption{An icosahedral-type domain embedded in a 4D subspace.}
	\label{fig:icosahedral-jr3d}
\end{figure}
