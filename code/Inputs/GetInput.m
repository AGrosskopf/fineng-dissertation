function [ inputData ] = GetInput( inputName )
%UNTITLED Returns the requested set of input
%   Detailed explanation goes here

% Get raw input params
switch inputName
    case 'JR'
        data = JoubertRogers;
        
    case 'JR3D'
        data = JoubertRogers3D;
        
    case 'JR2D'
        data = JoubertRogers2D;
        
    case 'PartJR2D'
        data = PartJR2D;
        PartJR2DSigVar
    case 'PartJR2DSigVar'
        data = PartJR2DSigVar;
    case 'PartJR3D'
        data = PartJR3D;
    otherwise
        error('Request for invalid set of input data');
end

% Compile J&R input data if applicable
if (strcmp(data.type,'JR'))
    inputData = JRCompile(data);
elseif (strcmp(data.type, 'PartJR'))
    inputData = PartJRCompile(data);
else
    inputData = data;
end

% Denote non-constant dimensions
inputData.constantDims = inputData.raw.dimSizes == 1;
inputData.dimCount = size(inputData.raw.dimSizes,2);
end

function inputData = JoubertRogers
% Raw input params
inputData.raw.S         = [70, 130];
inputData.raw.K         = 100;
inputData.raw.T         = [0.003, 1];
inputData.raw.r         = [0.01, 1];
inputData.raw.sigma     = [0.03, 0.6];
inputData.raw.delta     = [0, 1];

% Dim info
inputData.raw.dims      = 4; % 6 - {K, T}
inputData.raw.dimSizes  = [21 21 16 16];

% Indicate J&R 4D reduction
inputData.type          = 'JR';
end

function inputData = JoubertRogers3D
% Raw input params
inputData.raw.S         = [70, 130];
inputData.raw.K         = 100;
inputData.raw.T         = [0.003, 1];
inputData.raw.r         = 0.06;
inputData.raw.sigma     = [0.03, 0.6];
inputData.raw.delta     = 0;

% Dim info
inputData.raw.dims      = 4;
inputData.raw.dimSizes  = [21 21 16 1];

% Indicate J&R 4D reduction
inputData.type          = 'JR';
end


function inputData = PartJR3D
% Raw input params
inputData.raw.S         = [70, 130];
inputData.raw.K         = 100;
inputData.raw.T         = [0.003, 1];
inputData.raw.r         = 0.06;
inputData.raw.sigma     = [0.03, 0.6];
inputData.raw.delta     = 0;

% Dim info
inputData.raw.dims      = 5; % 6 - {K}
inputData.raw.dimSizes  = [21 21 1 16 1];

% Indicate J&R 4D reduction
inputData.type          = 'PartJR';
end

function inputData = JoubertRogers2D
% Raw input params
inputData.raw.S         = [70, 130];
inputData.raw.K         = 100;
inputData.raw.T         = [0.003, 1];
inputData.raw.r         = 0.06;
inputData.raw.sigma     = 0.4;
inputData.raw.delta     = 0;

% Dim info
inputData.raw.dims      = 4;
inputData.raw.dimSizes  = [60 30 1 1];

% Indicate J&R 4D reduction
inputData.type          = 'JR';
end

function inputData = PartJR2D
% Raw input params
inputData.raw.S         = [70, 130];
inputData.raw.K         = 100;
inputData.raw.T         = [0.003, 1];
inputData.raw.r         = 0.06;
inputData.raw.sigma     = 0.4;
inputData.raw.delta     = 0;

% Dim info
inputData.raw.dims      = 5; % 6 - {K}
inputData.raw.dimSizes  = [60 30 1 1 1];

% Indicate J&R 4D reduction
inputData.type          = 'PartJR';
end

function inputData = PartJR2DSigVar
% Raw input params
inputData.raw.S         = [20, 180];
inputData.raw.K         = 100;
inputData.raw.T         = 0.833021;
inputData.raw.r         = 1.9173/100;
inputData.raw.sigma     = [0.003, 1];
inputData.raw.delta     = 1.96;

% Dim info
inputData.raw.dims      = 5; % 6 - {K}
inputData.raw.dimSizes  = [60 1 1 30 1];

% Indicate J&R 4D reduction
inputData.type          = 'PartJR';
end

function inputData = JRCompile(inputData)

% Translate to JR 4D coordinates
inputData.SinvK      = inputData.raw.S./inputData.raw.K;
inputData.rT         = [min(inputData.raw.r) * min(inputData.raw.T), ...
                         max(inputData.raw.r) * max(inputData.raw.T)];
inputData.sigmaSqrT  = [min(inputData.raw.sigma)^2 * min(inputData.raw.T), ...
                         max(inputData.raw.sigma)^2 * max(inputData.raw.T)];
inputData.deltaT     = [min(inputData.raw.delta) * min(inputData.raw.T), ...
                         max(inputData.raw.delta) * max(inputData.raw.T)];
inputData.K          = 1;

% Report non-constant dimsizes
inputData.dimSizes   = inputData.raw.dimSizes(inputData.raw.dimSizes ~= 1);
                        
% Remove accidental duplicates
inputData.SinvK      = unique(inputData.SinvK)';
inputData.rT         = unique(inputData.rT);
inputData.sigSqrT    = unique(inputData.sigmaSqrT);
inputData.deltaT     = unique(inputData.deltaT);
end

function inputData = PartJRCompile(inputData)

% Translate to (part) JR 5D coordinates
inputData.SinvK      = inputData.raw.S./inputData.raw.K;
inputData.T          = inputData.raw.T;
inputData.r          = inputData.raw.r;
inputData.sigma      = inputData.raw.sigma;
inputData.delta      = inputData.raw.delta;
inputData.K          = 1;

% Report non-constant dimsizes
inputData.dimSizes   = inputData.raw.dimSizes(inputData.raw.dimSizes ~= 1);

% Remove accidental duplicates
inputData.SinvK      = unique(inputData.SinvK);
inputData.T          = unique(inputData.T);
inputData.r          = unique(inputData.r);
inputData.sigma      = unique(inputData.sigma);
inputData.delta      = unique(inputData.delta);

end