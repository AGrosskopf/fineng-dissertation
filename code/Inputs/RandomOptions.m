function [ randomOptions, randomOpVals ] = RandomOptions( inputData, N )
%RANDOMOPTIONS Generates a 6xN matrix of N random options

% Generate N rvs for each range
pts_S       = unifrnd(min(inputData.raw.S), max(inputData.raw.S), 1, N);
pts_K       = repmat(inputData.raw.K,1,N);
pts_T       = unifrnd(min(inputData.raw.T), max(inputData.raw.T), 1, N);
pts_r       = unifrnd(min(inputData.raw.r), max(inputData.raw.r), 1, N);
pts_sig     = unifrnd(min(inputData.raw.sigma), max(inputData.raw.sigma), 1, N);
pts_delta   = unifrnd(min(inputData.raw.delta), max(inputData.raw.delta), 1, N);
randomOpVals = [pts_S; pts_K; pts_T; pts_r; pts_sig; pts_delta]';

% Put into structs
randomOptions = cell(1,N);
for optionNo=1:N
    % Put params into struct
    opt.S = randomOpVals(optionNo,1);
    opt.K = randomOpVals(optionNo,2);
    opt.tau = randomOpVals(optionNo,3);
    opt.r = randomOpVals(optionNo,4);
    opt.sig = randomOpVals(optionNo,5);
    opt.delta = randomOpVals(optionNo,6);
    randomOptions{optionNo} = opt;
end

