% -- Warning: Serious abuse of function handles and anonymous functions...
function [MQ, TPS] = RBFImpVolFuncs()
% RBF related settings
rbfOptions.K = 5;
rbfOptions.eps = 7;
rbfOptions.smoothing = 0;
tpsOptions = rbfOptions; tpsOptions.basis = 'tps';
mqOptions = rbfOptions; mqOptions.basis = 'mq'; 


% Get part JR input data, lookup domain and table
inputParams = GetInput('PartJR2DSigVar');
scales = 2.^[-1 0:0.1:1.6]; n = 800;
[d t] = FibSpiralTable(inputParams, n, scales);
partJR = true;
ThreeD = false;
 
% Compute implied volatility using RBF MQ given market price
function impVol = MQFunc(modelParams, marketPrice)
    function PutPrice = MQFixed(sig)
        % Alter sigma
        altModel = modelParams;
        altModel.sig = sig;

        % Compute the put price under these params
        targetPt = DataSensitiveDimReduct(altModel, inputParams, partJR, ThreeD);
        PutPrice = RBFInterpolate(mqOptions, targetPt, d, t)*modelParams.K;
    end

    % Initial sigma guess = Brenner and Subrahmanyam (1988)
    tau = modelParams.tau;
    S = modelParams.S;
    modelParams.sig = 0.4; % needs a definition    
    sig0 = sqrt(2*pi/tau)*EuropeanCall(modelParams)/(S);

    % Find the root of f(sig) := MQFixed - marketPrice by varying sigma
    f = @(sig) MQFixed(sig) - marketPrice;
    impVol = fzero(f, sig0);
end

% Compute implied volatility using RBF TPS given market price
function impVol = TPSFunc(modelParams, marketPrice)
    function PutPrice = TPSFixed(sig)
        % Alter sigma
        altModel = modelParams;
        altModel.sig = sig;
        
        % Compute the put price under these params
        PutPrice = RBFInterpolate...
            (tpsOptions, DataSensitiveDimReduct(altModel, inputParams, partJR, ThreeD), ...
            d, t)*modelParams.K;
    end

    % Initial sigma guess = Brenner and Subrahmanyam (1988)
    tau = modelParams.tau;
    S = modelParams.S;
    modelParams.sig = 0.4; % needs a definition
    sig0 = sqrt(2*pi/tau)*EuropeanCall(modelParams)/(S);

    % Find the root of f(sig) := MQFixed - marketPrice by varying sigma
    f = @(sig) TPSFixed(sig) - marketPrice;
    impVol = fzero(f, sig0);
end

% Return handles to the above functions
MQ = @MQFunc;
TPS = @TPSFunc;
 
end