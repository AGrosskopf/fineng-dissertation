function surface = VolSurface(S0, r, delta, T, K, MarketPrice, ImpVolFn)
%**************************************************************************
% VolSurface - Volatility Surface
%   Compute the implied volatility of an underlying asset from the market 
%   values of European calls and plot the volatility surface.
%
%   surface = VolSurface(S0, r, T, K, CallPrice)
%
%==========================================================================
% INPUTS: 
%
%   S0      - Current price of the underlying asset.
%
%   r       - Annualized continuously compounded risk-free rate of return over
%           the life of the option, expressed as a positive decimal number.
%
%   T       - Vector of times to expiration of the option, expressed in years.
%
%   K       - Vector of strike (i.e., exercise) prices of the option.
%
%   MarketPrice   - Vector of prices (i.e., value) of options  from which 
%                   the implied volatilities of the underlying asset 
%                   are derived.
%
%   Note: the inputs T, K and Callprice must have the same length N. 
%   They form a set of N R^3 Vectors.
%   i.e. size([Maturity, Strike, CallPrice])= N x 3
%
%==========================================================================
% OUTPUTS:
%
%   Surface structure - Matrix of implied volatility of the underlying asset 
%   derived from European option prices.
%   
%   Surface plot      - 3D Implied volatility plot wtr moneyness and time to
%                       maturity
%      
%==========================================================================
% EXAMPLE:
%
%       see: Example1.m 
%            Example2.m         
%
%**************************************************************************
% Rodolphe Sitter - MSFM Student - The University of Chicago
% March 17, 2009
%**************************************************************************

% Set constant params for the model
modelParams.S = S0;
modelParams.r = r;
modelParams.delta = delta;

% COMPUTE THE IMPLIED VOLATILITIES
num = length(MarketPrice);
ImpliedVol = nan(num, 1);
for i = 1:length(ImpliedVol)
        % Set variable params for the model
        modelParams.K = K(i);
        modelParams.tau = T(i);
        marketPrice = MarketPrice(i);
        
        % Compute implied volatility
        ImpliedVol(i) = ImpVolFn(modelParams, marketPrice);
end


% CLEAN MISSING VALUES
M=K;              % strikes
IV=ImpliedVol;       % Implied Volatility
T=T(:); M=M(:); IV=IV(:);
missing=(T~=T)|(M~=M)|(IV~=IV);
T(missing)=[];
M(missing)=[];
IV(missing)=[];
% 

% CHOOSE BANDWIDTH hT and hM
hT=1;    surface.hT=hT;
hM=median(abs(M-median(M)));    surface.hM=hM;
% CHOOSE GRID STEP N 
TT = sort(T);     MM = sort(M);
NT = histc(T,TT); NM = histc(M,MM);
NT(NT==0) = [];   NM(NM==0) = [];
nt=length(NT);    nm=length(NM);
N=min(max(nt,nm),70);


% SMOOTHE WITH GAUSSIAN KERNEL 
kerf=@(z)exp(-z.*z/2)/sqrt(2*pi);
surface.T=linspace(min(T),max(T),N);
surface.M=linspace(min(M),max(M),N);
surface.IV=nan(1,N);
for i=1:N
    for j=1:N
    z=kerf((surface.T(j)-T)/hT).*kerf((surface.M(i)-M)/hM); 
    surface.IV(i,j)=sum(z.*IV)/sum(z);
    end
end


% PLOT THE VOLATILITY SURFACE
% surf(surface.T,surface.M,surface.IV)
% axis tight; grid on;
% xlabel('$T$','Fontsize',16,'FontWeight','Bold','interpreter','latex');
% ylabel('$K$','Fontsize',16,'FontWeight','Bold','interpreter','latex');
% zlabel('$\sigma_{imp}$','Fontsize',16,'FontWeight','Bold','interpreter','latex');
% set(gca,'Fontsize',16,'FontWeight','Bold','LineWidth',2);

