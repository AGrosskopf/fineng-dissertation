%% Data
% General data (ticker-invariant)
dataDir='C:\Users\Aryeh\Documents\fineng-dissertation\data\CBOE\final\';
tickers=[{'AAPL'} {'GS'} {'IBM'} {'RDSA'} {'WMT'}];
%tickers=[{'RDSA'} ];
spots=[105.92 157.6 147.09 49.46 66.95]; % spot prices corresponding to above tickers
divs=[1.96 1.66 3.5 7.69 2.88]./100; % dividend rates corresponding to above tickers
r=1.9173/100; % risk-free rate taken as US treasury 10-year bond

% Load the data
ticker = tickers{1};
dataFile=strcat(dataDir, ticker, '.csv');
optionData=csvread(dataFile,1,0); % skip header line

% Extract P(S,tau;K), tau and K for tau in input data
correctTauRows = optionData(:,2) == 0.833021;
P=optionData(correctTauRows,1);
Tau=optionData(correctTauRows,2);
K=optionData(correctTauRows,3);
    
%% Set market conditions 
expiries=Tau;
strikes=K;
marketVs=P;
r = 0.06;
S0 = spots(1);
delta = divs(1);

%% Get MQ and TPS imp vol functions
[MQImpVol, TPSImpVol] = RBFImpVolFuncs();

%% Vol surfaces
% MQ volatility surface
tic
MQSurf = VolSurface(S0, r, delta,expiries,strikes,marketVs, MQImpVol);
mqTime = toc

% TPS volatility surface
tic
TPSSurf = VolSurface(S0, r, delta,expiries,strikes,marketVs, TPSImpVol);
tpsTime = toc

% LR Binomial volatility surface
tic
LRBinSurf = VolSurface(S0, r, delta,expiries,strikes,marketVs, @LRBImpliedVolatility);
lrbTime = toc

%% LR binomial volatility surface + difference
plot(MQSurf.M,MQSurf.IV(:,1)); hold on;
plot(TPSSurf.M,TPSSurf.IV(:,1)); hold on;
plot(LRBinSurf.M,LRBinSurf.IV(:,1)); hold off;
[h, ~] = legend('MQ', 'TPS', 'LR BIN');
axis tight;
xlabel('$K$','Fontsize',16,'interpreter','latex');
ylabel('$\sigma_{imp}$','Fontsize',16,'interpreter','latex');

figure;
plot(MQSurf.M,abs(MQSurf.IV(:,1)-LRBinSurf.IV(:,1))); hold on;
plot(TPSSurf.M,abs(TPSSurf.IV(:,1)-LRBinSurf.IV(:,1))); hold off;
[h, ~] = legend('MQ', 'TPS');
axis tight;
xlabel('$K$','Fontsize',16,'interpreter','latex');
ylabel('$|\sigma_{imp}-\hat{\sigma_{imp}}|$','Fontsize',16,'interpreter','latex');