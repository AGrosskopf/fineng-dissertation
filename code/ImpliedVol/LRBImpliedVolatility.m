function impVol = LRBImpliedVolatility(modelParams, marketPrice )
%LRBImpliedVolatility Computes the volatility implied by the given market
%  price of an American put option under the L-R Binomial Method

% Function to compute L-R Binomial with all model params fixed except sigma
    function PutPrice = LRBinFixed (sig)
        % Alter sigma
        altModel = modelParams;
        altModel.sig = sig;
        
        % Compute the put price / critical stock price under these params
        PutPrice = LRBinomial(altModel);
    end

    % Initial sigma guess = Brenner and Subrahmanyam (1988)
    tau = modelParams.tau;
    S = modelParams.S;
    modelParams.sig = 0.4; % needs a definition
    sig0 = sqrt(2*pi/tau)*EuropeanCall(modelParams)/(S);

    % Find the root of f(sig) := LRBinFixed - marketPrice by varying sigma
    f = @(sig) LRBinFixed(sig) - marketPrice;
    impVol = fzero(f, sig0);
end

