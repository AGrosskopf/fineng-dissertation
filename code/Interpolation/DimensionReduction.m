function [ reducedPoint ] = DimensionReduction( option, inputData )
%REDUCEDIMENSION Converts the option struct to its 4d reduced form
%   See Joubert and Rogers for details on the 6D -> 4D reduction

% Extract the paramters for the model
S = option.S;
K = option.K;
delta = option.delta;
T = option.tau;
r = option.r;
sig = option.sig;

% Prune points from constant dimensions
if (strcmp(inputData.type,'JR'))
    % Translate to (S/K, rT, sigmaSqrT, deltaT) dimensions
    SinvK = S / K;
    rT = r*T;
    sigmaSqrT = sig*sig*T;
    deltaT = delta*T;
    reducedPoint = [SinvK rT sigmaSqrT deltaT];
elseif(strcmp(inputData.type,'PartJR'))
    SinvK = S / K;
    reducedPoint = [SinvK, T, r, sig, delta];
else
    error('Called DimensionReduction on non-JR data');
end
    
end


