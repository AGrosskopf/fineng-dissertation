function approximation = MtlInterpolate3( interpOptions, method, point, domain, table )
%MtlInterpolate Wrapper around matlab 3d interpolation

% Choose K points in each dimension
Knn = interpOptions.K;
nearestXI = knnsearch(domain.xGrid', point(1), 'K', Knn);
nearestYI = knnsearch(domain.yGrid', point(2), 'K', Knn);
nearestZI = knnsearch(domain.zGrid', point(2), 'K', Knn);
nearestX  = sort(domain.xGrid(nearestXI));
nearestY  = sort(domain.yGrid(nearestYI));
nearestZ  = sort(domain.zGrid(nearestZI));

% Create 2d lookup grid for above points
V = zeros(length(nearestY), length(nearestX), length(nearestZ));
for i = 1:Knn
    z = nearestZ(i);
    for j = 1:Knn
        y = nearestY(j);
        for k = 1:Knn
            x = nearestX(k);
            tableI = domain.tuples(:,1) == x & domain.tuples(:,2) == y  ...
                    & domain.tuples(:,3) == z;
            if sum(tableI) ~= 1
                error('failed to find unique lookup point')
            end
            V(i, j, k) = table.vals(tableI);
        end
    end
end

% Do matlab interpolate
approximation = interp3(nearestX, nearestY, nearestZ, V, point(1), point(2), point(3), method);

end
