function targetPt =  DataSensitiveDimReduct (modelParams, inputData, partJR, ThreeD) 
    % Extract relevant points
    fullTargetPt = DimensionReduction(modelParams, inputData);
    if partJR && ThreeD
        targetPt = fullTargetPt([1,2,4]);
    elseif ~partJR && ThreeD
        targetPt = fullTargetPt([1,2,3]);
    elseif ~ThreeD
        targetPt = fullTargetPt(~inputData.constantDims);
    end
end