function phi = GetRBFVector( rbf, x, nodes, eps )
%GETRBFVECTOR Returns the RBF col-vector of phi(x - x_i) for target point x

switch lower(rbf)
    case 'mq'
        % MQ RBF
        MQ = @(xi) sqrt(norm(x - xi)^2 + eps^2);
        phi = PopulateVector(nodes, MQ);
    case 'tps'
        % TPS RBF
        TPS = @(xi) norm(x - xi)^2*log(norm(x - xi) + 1);
        phi = PopulateVector(nodes, TPS);
    otherwise
        error('Unknown RBF function requested');
end

end

function phi = PopulateVector(nodes, rbf)

% Fill vector
nodeCount   = size(nodes,1);
phi         = zeros(nodeCount, 1); % pre-allocate
for i = 1:nodeCount
    phi(i) = rbf(nodes(i,:));
end

end