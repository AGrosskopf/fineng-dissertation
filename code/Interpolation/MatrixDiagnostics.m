function unstable = MatrixDiagnostics( A, print )
%MATRIXDIAGNOSTICS Summarise key stats related to matrix inversion of A

% Diagnostics
TOL = 9e-16;
invCond = rcond(A);
unstable = invCond < TOL;

% Print results 
if (exist('print', 'var') && print)
    fprintf('Rcond(A) \t %0.6f \n', invCond);
    fprintf('Rank(A) \t %0.6f \n', rank(A));
end

end

