function approximation = MtlInterpolate2( interpOptions, method, point, domain, table )
%MtlInterpolate Wrapper around matlab 2d interpolation

% Choose K points in each dimension
Knn = interpOptions.K;
nearestXI = knnsearch(domain.xGrid', point(1), 'K', Knn);
nearestYI = knnsearch(domain.yGrid', point(2), 'K', Knn);
nearestX  = sort(domain.xGrid(nearestXI));
nearestY  = sort(domain.yGrid(nearestYI));

% Create 2d lookup grid for above points
V = zeros(length(nearestY), length(nearestX));
for i = 1:Knn
    y = nearestY(i);
    for j = 1:Knn
        x = nearestX(j);
        tableI = domain.tuples(:,1) == x & domain.tuples(:,2) == y;
        if sum(tableI) ~= 1
            error('failed to find unique lookup point')
        end
        V(i, j) = table.vals(tableI);
    end
end

% Do matlab interpolate
approximation = interp2(nearestX, nearestY, V, point(1), point(2), method);

end
