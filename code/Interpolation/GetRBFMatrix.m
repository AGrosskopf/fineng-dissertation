function A = GetRBFMatrix( rbf, nodes, eps )
%GETRBFMATRIX Generates the matrix A in the RBF interpolation Ax = b

switch lower(rbf)
    case 'mq'
        % MQ RBF
        MQ = @(x1, x2) sqrt(norm(x1 - x2)^2 + eps^2);
        A = PopulateMatrix(nodes, MQ);
    case 'tps'
        % TPS RBF definition
        TPS = @(x1, x2) norm(x1 - x2)^2*log(norm(x1 - x2) + 1);
        A = PopulateMatrix(nodes, TPS);
    otherwise
        error('Unknown RBF function requested');
end

end

function A = PopulateMatrix(nodes, rbf)

% Fill lower triangle of A
nodeCount   = size(nodes,1);
A           = zeros(nodeCount); % pre-allocate
for i = 1:nodeCount
    for j = 1:i
        A(i,j) = rbf(nodes(i,:), nodes(j,:));
    end
end

% Copy lower triangle to upper triangle
A = tril(A,-1)' + A;

end