function approximation = RBFInterpolate( rbfOptions, x, domain, table )
%RBFInterpolate Performs RBF interpolation 
%   rbfOptions      - Settings for this function
%   x               - The point to approximate (row vector)
%   domain          - The domain of the lookup table
%   table           - The values of the lookup table

% Get K nearest neighbours
nodesI = knnsearch(domain.tree, x, 'K', rbfOptions.K);
nodesTuples = domain.tuples(nodesI,:);

% Get matrix A and apply smoothing
A = GetRBFMatrix(rbfOptions.basis, nodesTuples, rbfOptions.eps);
A = A + rbfOptions.smoothing; 

% Append polynomial
P = ones(rbfOptions.K,1);
A = [ A  P;
      P' 0];

% Get matrix b
b = [table.vals(nodesI); 0];

% Find c = A^(-1) b
c = A\b;

% Extract alpha
alpha = c(end); c = c(1:end-1);

% Get distances vector
phi = GetRBFVector(rbfOptions.basis, x, nodesTuples, rbfOptions.eps );

% Find approximation = sum(c_i * x_i)
approximation = phi'*c + alpha;

end