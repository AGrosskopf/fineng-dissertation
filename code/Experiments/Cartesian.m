warning off;

% RBF related settings
rbfOptions.K = 5;
rbfOptions.eps = 7;
rbfOptions.smoothing = 0;
tpsOptions = rbfOptions; tpsOptions.basis = 'tps';
mqOptions = rbfOptions; mqOptions.basis = 'mq'; 

% Matlab interpolation options
mtlOptions.K = 5;

% Number of tests
N = 3000;

%% JR 2D Test 
% Get input data, lookup domain and table
inputParams = GetInput('JR2D');
[d t] = CartesianTable(inputParams);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions, mtlOptions);

%% Part JR 2D Test 
% Get input data, lookup domain and table
inputParams = GetInput('PartJR2D');
[d t] = CartesianTable(inputParams);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions, mtlOptions);

%% JR 3D Test 
% Get input data, lookup domain and table
inputParams = GetInput('JR3D');
[d t] = CartesianTable(inputParams);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions, mtlOptions);

%% Part JR 3D Test 
% Get input data, lookup domain and table
inputParams = GetInput('PartJR3D');
[d t] = CartesianTable(inputParams);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions, mtlOptions);