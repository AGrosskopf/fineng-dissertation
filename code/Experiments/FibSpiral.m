warning off;

% RBF related settings
rbfOptions.K = 5;
rbfOptions.eps = 5;
rbfOptions.smoothing = 0;
tpsOptions = rbfOptions; tpsOptions.basis = 'tps';
mqOptions = rbfOptions; mqOptions.basis = 'mq'; 

% Number of tests
N = 3000;

% Get JR input data, lookup domain and table
inputParams = GetInput('JR2D');
scales = 2.^[-1 0:0.1:1.6]; n = 800;
[d t] = FibSpiralTable(inputParams, n, scales);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions);

% Get part JR input data, lookup domain and table
inputParams = GetInput('PartJR2D');
scales = 2.^[-1 0:0.1:1.6]; n = 800;
[d t] = FibSpiralTable(inputParams, n, scales);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions);