function PerformExperiments( N, domain, table, inputData, mqOptions, tpsOptions, mtlOptions )

% Assume mlOptions is provided iff Cartesian Grid
cartesianGrid = exist('mtlOptions', 'var');

% Check if 2d or 3d
ThreeD = sum(~inputData.constantDims) == 3;
if (ThreeD)
    MtlInterpolate = @MtlInterpolate3;
else
    MtlInterpolate = @MtlInterpolate2;
end    

% Check JR or PartJR
partJR = strcmp(inputData.type, 'PartJR');

% Initialise metrics
z = zeros(N, 2);
mqErrorTotal = z; tpsErrorTotal = z;
lrbTotalTime = 0; mqTotalTime = 0; tpsTotalTime = 0;
if cartesianGrid
    mtlLinErrorTotal = z; mtlNNErrorTotal = z;
    mtlCubicErrorTotal = z; mtlSplineErrorTotal = z;
    mtlLinTotalTime = 0; mtlNNTotalTime = 0;
    mtlCubicTotalTime = 0; mtlSplineTotalTime = 0;
end


% Test RBF on random options
randomOpts = RandomOptions(inputData, N);
for optNo = 1:N
    opt = randomOpts{optNo};
    if (N < 20)
        fprintf('\n--------  NEW OPTION --------\n');
        fprintf('r = %0.4f, d = %0.4f, tau = %0.6f, sig = %0.4f\n', opt.r, opt.delta, opt.tau, opt.sig);
    elseif mod(optNo, 100) == 0 
        fprintf('%d of %d\n', optNo, N);
    end
    
    tic
    lrbVal = LRBinomial(opt);
    lrbTime = toc; lrbTotalTime = lrbTotalTime + lrbTime;

    if (lrbVal < 0.05)
        continue
    end
    
    % Extract relevant points
    fullTargetPt = DimensionReduction(opt, inputData);
    if partJR && ThreeD
        targetPt = fullTargetPt([1,2,4]);
    elseif ~partJR && ThreeD
        targetPt = fullTargetPt([1,2,3]);
    elseif ~ThreeD
        targetPt = fullTargetPt([1,2]);
    end

    % MQ RBF interpolation
    tic
    rbfVal = RBFInterpolate(mqOptions, targetPt, domain, table)*100;
    absError = abs(lrbVal - rbfVal);  relError = (lrbVal - rbfVal)/lrbVal;
    mqErrorTotal(optNo,1) = absError; mqErrorTotal(optNo,2) = relError;
    mqTime = toc; mqTotalTime = mqTotalTime + mqTime;

    % TPS RBF interpolation
    tic
    rbfVal = RBFInterpolate(tpsOptions, targetPt, domain, table)*100;
    absError = abs(lrbVal - rbfVal); relError = (lrbVal - rbfVal)/lrbVal;
    tpsErrorTotal(optNo,1) = absError; tpsErrorTotal(optNo,2) = relError;
    tpsTime = toc; tpsTotalTime = tpsTotalTime + tpsTime;
    
    % Test matlab interpolation if cartesian grid
    if cartesianGrid
        % Linear
        tic
        mtlVal = MtlInterpolate(mtlOptions, 'linear', targetPt, domain, table)*100;
        absError = abs(lrbVal - mtlVal); relError = (lrbVal - mtlVal)/lrbVal;
        mtlLinErrorTotal(optNo,1) = absError; mtlLinErrorTotal(optNo,2) = relError;
        mtlTime = toc; mtlLinTotalTime = mtlLinTotalTime + mtlTime;
        
        % NN
        tic
        mtlVal = MtlInterpolate(mtlOptions, 'nearest', targetPt, domain, table)*100;
        absError = abs(lrbVal - mtlVal); relError = (lrbVal - mtlVal)/lrbVal;
        mtlNNErrorTotal(optNo,1) = absError; mtlNNErrorTotal(optNo,2) = relError;
        mtlTime = toc; mtlNNTotalTime = mtlNNTotalTime + mtlTime;
                
        % Cubic
        tic
        mtlVal = MtlInterpolate(mtlOptions, 'cubic', targetPt, domain, table)*100;
        absError = abs(lrbVal - mtlVal); relError = (lrbVal - mtlVal)/lrbVal;
        mtlCubicErrorTotal(optNo,1) = absError; mtlCubicErrorTotal(optNo,2) = relError;
        mtlTime = toc; mtlCubicTotalTime = mtlCubicTotalTime + mtlTime;
        
        % Spline
        tic
        mtlVal = MtlInterpolate(mtlOptions, 'spline', targetPt, domain, table)*100;
        absError = abs(lrbVal - mtlVal); relError = (lrbVal - mtlVal)/lrbVal;
        mtlSplineErrorTotal(optNo,1) = absError; mtlSplineErrorTotal(optNo,2) = relError;
        mtlTime = toc; mtlSplineTotalTime = mtlSplineTotalTime + mtlTime;
    end
    
    % Print detail if small test
    if (N < 20)
        fprintf('LRBin  \t %0.6f \t', lrbVal);
        fprintf('MQ RBF \t %0.6f \t', rbfVal);
        fprintf('TPS RBF\t %0.6f\n', rbfVal);
        if cartesianGrid
            fprintf('Matlab \t %0.6f\n', mtlVal);
        end
    end
    
    
end

% Define error matrix
MRE     = @(e) sum(e(:,2))/N;
RMSE    = @(e) sqrt(sum(e(:,2).^2)/N);
MaxR    = @(e) max(abs(e(:,2)));
MaxA    = @(e) max(e(:,1));
metrics = containers.Map; 
metrics('MRE') = MRE; metrics('RMSE') = RMSE;
metrics('MaxR') = MaxR; metrics('MaxA') = MaxA;

% Compute metrics for each interpolation method
for metric = metrics.keys
    fprintf('\n---- %s ----\n', metric{1});
    metricFunc = metrics(metric{1});
    fprintf('MQ \t\t %0.6f \n', metricFunc(mqErrorTotal));
    fprintf('TPS\t\t %0.6f \n', metricFunc(tpsErrorTotal));
    if cartesianGrid
        fprintf('MTL Linear\t %0.6f \n', metricFunc(mtlLinErrorTotal));
        fprintf('MTL NN    \t %0.6f \n', metricFunc(mtlNNErrorTotal));
        fprintf('MTL Cubic \t %0.6f \n', metricFunc(mtlCubicErrorTotal));
        fprintf('MTL Spline\t %0.6f \n', metricFunc(mtlSplineErrorTotal));
    end
end

fprintf('\n---- Time ----\n');
fprintf('LRB Total Time \t %0.6f \n', lrbTotalTime);
fprintf('MQ Total Time \t %0.6f \n', mqTotalTime);
fprintf('TPS Total Time\t %0.6f \n', tpsTotalTime);
if cartesianGrid
    fprintf('MTL Linear Total Time\t %0.6f \n', mtlLinTotalTime);
    fprintf('MTL NN Total Time\t %0.6f \n', mtlNNTotalTime);
    fprintf('MTL Cubic Total Time\t %0.6f \n', mtlCubicTotalTime);
    fprintf('MTL Spline Total Time\t %0.6f \n', mtlSplineTotalTime);
end

end

