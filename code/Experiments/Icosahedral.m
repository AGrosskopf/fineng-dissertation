warning off;

% RBF related settings
rbfOptions.K = 30;
rbfOptions.eps = 0.00000000000000000000000001;
rbfOptions.smoothing = 0;
tpsOptions = rbfOptions; tpsOptions.basis = 'tps';
mqOptions = rbfOptions; mqOptions.basis = 'mq'; 

% Test count
N = 3000;


%% JR 3d
% Get input data, lookup domain and table
inputParams = GetInput('JR3D');
scales = [-3:0.2:-2 -1.5:0.5:1]; n = 2;
[d t] = IcosahedralTable(inputParams, n, scales);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions);

%% Part JR 3d
% Get input data, lookup domain and table
inputParams = GetInput('PartJR3D');
scales = [-3:0.2:-2 -1.5:0.5:1]; n = 2;
[d t] = IcosahedralTable(inputParams, n, scales);

% Do test
PerformExperiments(N, d, t, inputParams, mqOptions, tpsOptions);