function [price, EE] = LRBinomial(modelParams)
% L-R Binomial Method for pricing American put options

% Extract the paramters for the model
S = modelParams.S;
K = modelParams.K;
delta = modelParams.delta;
tau = modelParams.tau;
r = modelParams.r;
sig = modelParams.sig;

% Tolerance for early exercise boundary
TOL = 1e-10;

% The cost of carry is just the risk free rate - dividend rate
b = r - delta;

% Apply payoff if reached expiry
if tau <= 0
    price = max(K - S, 0);
    EE = price == K - S;
    return;
end

% We fix the number of nodes per day and then derive the change in time
% between adjacent nodes and the required number of iterations
levels = 4999;
dt = tau/levels;
iterations = levels;

% Ensure the number of iterations is odd
iterations = iterations + 1 - (mod(iterations,2));

% Compute the points d1 and d2 as defined in the BSE
D1 = d1(modelParams);
D2 = d2(modelParams);

% N(0,1) cummulative distribution approximation
fh = @(z,n)(0.5+sign(z)...
    .*sqrt(0.25-0.25*exp(-((z/(n+1/3+0.1/(n+1))).^2)*(n+1/6))));

% Compute the parameters for the LR binomial model
pBar = fh(D1, iterations);
p = fh(D2, iterations);
upJump = exp(r * dt) * pBar / p;
downJump = (exp(r * dt) - p * upJump) / (1 - p);

% The underlying price  follows a discretized brownian motion process
% and each possibility at each step is recorded in a binary tree
underlyingTree = nan(levels + 1,levels + 1);
underlyingTree(1,1) = S; % initial price
for level = 2:levels + 1
    % All but one of the price nodes at this new level are up jumps from 
    % all the price nodes from the previous level
    underlyingTree(1:level - 1, level) ... 
        = underlyingTree(1:level - 1, level - 1) * upJump;
    
    % The final node is a down jump from the lowest price node from the
    % previous level
    underlyingTree(level, level) ...
        = underlyingTree(level - 1, level - 1) * downJump;
end

% Calculate the value at expiry: P(S,T) = max(K - S, 0)
optionTree = nan(levels + 1,levels + 1);
optionTree(:,end) = max(K - underlyingTree(:,end), 0);

% Loop backwards to get values at the earlier nodes
for level = iterations:-1:1
    % Discount the expected value of the option at the 2 subsequent value 
    % nodes by the risk free rate
    optionTree(1:level,level) ...
        = exp(-r * dt) ... % Discount factor
        * ( p      * optionTree(1:level, level + 1) + ...
           (1 - p) * optionTree(2:level + 1, level + 1));
       
    % Apply payoff function at this level
    optionTree(1:level,level) ...
        = max(K - underlyingTree(1:level, level), optionTree(1:level, level));
end

% Output the option price
price = optionTree(1);    
EE = price - (K - S) < TOL;