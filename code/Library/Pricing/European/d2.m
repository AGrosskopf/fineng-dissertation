function d2 = d2(modelParams)
%D2 Computes the parameter d2 in the BSE
% Reference: Li(2010b) eq (27)

% Extract the paramters for the model
S = modelParams.S;
K = modelParams.K;
delta = modelParams.delta;
tau = modelParams.tau;
r = modelParams.r;
sig = modelParams.sig;

% The cost of carry is just the risk free rate - dividend rate
b = r - delta;

volExp = sig*sqrt(tau);
d2 = d1(modelParams) - volExp;
end

