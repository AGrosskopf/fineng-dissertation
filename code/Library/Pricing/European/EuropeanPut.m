function price = EuropeanPut(modelParams)
%EUROPEANPUT Computes the price of a European call option under the BS model

% Extract the paramters for the model
S = modelParams.S;
K = modelParams.K;
delta = modelParams.delta;
tau = modelParams.tau;
r = modelParams.r;

% The cost of carry is just the risk free rate - dividend rate
b = r - delta;

% Ignore negative S values (fzero may pose S < 0)
if S < 0
    modelParams.S = 0;
    S = 0;
end

% Compute the probability of -d1(S,K) and -d2(S,K) in the N(0,1) dist.
D1 = d1(modelParams);
D2 = d2(modelParams);
probD1 = normcdf(-D1, 0, 1);
probD2 = normcdf(-D2, 0, 1);

% Compute the price
price = K*exp(-r*tau)*probD2 - S*exp((b-r)*tau)*probD1;

end

