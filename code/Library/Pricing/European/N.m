function normCDF = N( x )
%N Finds N(x) := P(X < x) where X~N(0,1) (the standard normal dists' cdf)

% N(0,1) cummulative distribution approximation
fh = @(z,n)(0.5+sign(z)...
    .*sqrt(0.25-0.25*exp(-((z/(n+1/3+0.1/(n+1))).^2)*(n+1/6))));

n=1000; % Sufficiently large for most applications
normCDF = fh(x,n);
end

