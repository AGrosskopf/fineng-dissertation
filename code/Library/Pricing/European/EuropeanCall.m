function price = EuropeanCall(modelParams)
%EUROPEANPUT Computes the price of a European put option under the BS model
% Reference: Li(2010b) (25)

% Extract the paramters for the model
S = modelParams.S;
K = modelParams.K;
tau = modelParams.tau;
r = modelParams.r;

% Compute the price of a European put
eurPut = EuropeanPut(modelParams);

% Use PCP to derive the European call price
price = eurPut + S - K*exp(-r*tau);

end

