function d1 = d1(modelParams)
%D1 Computes the parameter d1 in the BSE
% Reference: Li(2010b) eq (26)

% Extract the paramters for the model
S = modelParams.S;
K = modelParams.K;
delta = modelParams.delta;
tau = modelParams.tau;
r = modelParams.r;
sig = modelParams.sig;

% The cost of carry is just the risk free rate - dividend rate
b = r - delta;

volExp = sig*sqrt(tau);
d1 = log(S*exp(b*tau)/K) / volExp + volExp/2;
end

