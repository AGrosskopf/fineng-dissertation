% Script-like function
function [lookupDomain, lookupTable] = FibSpiralTable(inputData, n, scales)

% Check we are using a 2D domain
if (sum(~inputData.constantDims) ~= 2)
    error('Must be a 2D input');
end

% Check subspace type
if (strcmp(inputData.type, 'JR'))
    xRange = inputData.SinvK;   xLabel = '$\frac{S}{K}$';
    yRange = inputData.rT;      yLabel = '$rT$';
elseif (strcmp(inputData.type, 'PartJR'))
    xRange = inputData.SinvK;   xLabel = '$\frac{S}{K}$';
    if (inputData.constantDims(2))
        yRange = inputData.sigma;       yLabel = '$\sigma$';
    else
        yRange = inputData.T;       yLabel = '$T$';
    end
else
    error('Not yet implemented');
end

% Generate the shape
lookupDomain = GenerateDomain( xRange, yRange, n, scales );
lookupTable = GenerateTable( inputData, lookupDomain );

% Illustrate shape
figure('color','w'); hold on;
scatter(lookupDomain.tuples(:,1), lookupDomain.tuples(:,2));
xlabel(xLabel,'Interpreter','LaTex');
ylabel(yLabel,'Interpreter','LaTex');
hold off;
end

function lookupDomain = GenerateDomain( xRange, yRange, n, scales )

% Set centres
xCentre             = mean(xRange);
yCentre             = mean(yRange);
shapeCentre              = [xCentre yCentre];

% Set semi axis lengths
saLength            = @(range) (range(2) - range(1)) / 2;
xLength             = saLength(xRange);
yLength             = saLength(yRange);
shapelength         = [xLength yLength];

% Generate spiral
r = max(shapelength);
R = r*sqrt(linspace(1/2,n-1/2,n))/sqrt(n-1/2);
T = 4/(1+sqrt(5))*pi*(1:n);
xi = R.*cos(T);
yi = R.*sin(T);
originShape = [xi', yi'];

% Create scaled duplicates
concatShape = [];
for scale = scales
    newHex              = scale2D(originShape, scale*[1 1]);
    concatShape           = [concatShape; newHex];
end

% Centre and scale
scaledShape = scale2D(concatShape, shapelength);
centredShape = translate2D(scaledShape, shapeCentre);

% Record all tuples
lookupDomain.tuples = unique(centredShape,'rows');

% Build a KD-tree
lookupDomain.tree = KDTreeSearcher(lookupDomain.tuples);

end

function translated2D = translate2D(points, translationVec)
M = makehgtform('translate',[translationVec 0]); % expect 2D input
pointCount = size(points,1);
translated2D = (M*[points zeros(pointCount, 1) ones(pointCount, 1)]')';
translated2D = translated2D(:,1:2);
end

function scaled2D = scale2D(points, scaleVec)
M = makehgtform('scale',[scaleVec 1]);
pointCount = size(points,1);
scaled2D = (M*[points zeros(pointCount, 1) ones(pointCount, 1)]')';
scaled2D = scaled2D(:,1:2);
end