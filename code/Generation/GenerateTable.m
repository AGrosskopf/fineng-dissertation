function [lookupTable] = GenerateTable( inputData, lookupDomain )
%GENERATETABLE Generates a lookup table of American option prices given a
% domain. Also indicates early-exercise subset of the lookup domain.
%  Generates a lookup table for use in American option pricing . The lookup
%  domain should be in 4 dimensions, see Joubert and Rogers (1999). 

% Create empty lookup table
totalValues = size(lookupDomain.tuples,1);
percentPoints = totalValues / 100;
lookupTable.vals = zeros(totalValues, 1);
lookupTable.EE = false(totalValues, 1);

% For each node in grid
fprintf('Progress:\n');
fprintf(['\n' repmat('.',1, 100) '\n\n']);
for  valuesComputed=1:totalValues    
    % a = S0/K, b = rT, c = sigSqrT, d = deltaT
    opt = loadOption(inputData, lookupDomain, valuesComputed);

    % Compute American option value
    [lookupValue, EE] = LRBinomial(opt);
    lookupTable.vals(valuesComputed) = lookupValue;
    lookupTable.EE(valuesComputed) = EE;
    
    % Update on progress
    if (rem(valuesComputed, percentPoints) < 1)
      fprintf('\b|\n');
    end
end

end

function opt = loadOption(inputData, lookupDomain, tupleNo)
if strcmp(inputData.type,'JR')
    % Skip over constant dims
    dimsSkipped = 0;
    
    if ~inputData.constantDims(1)
        a = lookupDomain.tuples(tupleNo, 1);
    else
        a = inputData.SinvK(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(2)
        b = lookupDomain.tuples(tupleNo, 2 - dimsSkipped);
    else
        b = inputData.rT(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(3)
        c = lookupDomain.tuples(tupleNo, 3 - dimsSkipped);
    else
        c = inputData.sigSqrT(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(4)
        d = lookupDomain.tuples(tupleNo, 4 - dimsSkipped);
    else
        d = inputData.deltaT(1);
        dimsSkipped = dimsSkipped + 1;
    end
    
    % Sanitize params for LRBin
    if a <= 0
        a = 0;
    end
    if b < 0 || d < 0
        % Treat as if tau = 0, IE exercise
        c = 0;
    end
  
    % Collect into parameters struct
    % We have p(a,b,c,d) := P(a, 1, b/c, 1, c, d/c)
    opt = struct('S', a, 'K', 1, 'r', b/c, 'sig', 1, 'tau', c, 'delta', d/c);
    
elseif strcmp(inputData.type,'PartJR');
    % Skip over constant dims
    dimsSkipped = 0;
    
    % K is static
    K = inputData.K;
    
    if ~inputData.constantDims(1)
        S = lookupDomain.tuples(tupleNo, 1);
    else
        S = inputData.SinvK(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(2)
        T = lookupDomain.tuples(tupleNo, 2 - dimsSkipped);
    else
        T = inputData.T(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(3)
        r = lookupDomain.tuples(tupleNo, 3 - dimsSkipped);
    else
        r = inputData.r(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(4)
        sig = lookupDomain.tuples(tupleNo, 4 - dimsSkipped);
    else
        sig = inputData.sigma(1);
        dimsSkipped = dimsSkipped + 1;
    end
    if ~inputData.constantDims(5)
        delta = lookupDomain.tuples(tupleNo, 5 - dimsSkipped);
    else
        delta = inputData.delta(1);
        dimsSkipped = dimsSkipped + 1;
    end
    
    % Sanitize params for LRBin
    if sig < 0
        sig = 0;
    end
    if delta < 0
        delta = 0;
    end
    if r < 0
        r = 0;
    end
    
    % Collect into parameters struct
    % We have p(a,b,c,d) := P(a, 1, b/c, 1, c, d/c)
    opt = struct('S', S, 'K', K, 'r', r, 'sig', sig, 'tau', T, 'delta', delta);
else
    error('Implement non JR input');
end

end

