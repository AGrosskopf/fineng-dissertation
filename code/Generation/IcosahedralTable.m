% Script-like function
function [lookupDomain, lookupTable] = IcosahedralTable(inputData, icoRefinements, scales)

% Check we are using a 3D domain
if (sum(~inputData.constantDims) ~= 3)
    error('Must be a 3D input');
end

% Check subspace type
if (strcmp(inputData.type, 'JR'))
    xRange = inputData.SinvK;       xLabel = '$\frac{S}{K}$';
    yRange = inputData.rT;          yLabel = '$rT$';
    zRange = inputData.sigmaSqrT;   zLabel = '$\sigma^2 T$';
elseif (strcmp(inputData.type, 'PartJR'))
    xRange = inputData.SinvK;       xLabel = '$\frac{S}{K}$';
    yRange = inputData.T;           yLabel = '$T$';
    zRange = inputData.sigma;       zLabel = '$\sigma$';
else
    error('Not yet implemented');
end

% Generate the shape
lookupDomain = GenerateDomain( xRange, yRange, zRange, icoRefinements, scales );
lookupTable = GenerateTable( inputData, lookupDomain );

% Illustrate icosahedral
figure('color','w'); hold on;
h=trimesh(lookupDomain.TR);
set(h,'EdgeColor','b','FaceColor','w') 
xlabel(xLabel,'Interpreter','LaTex');
ylabel(yLabel,'Interpreter','LaTex');
zlabel(zLabel,'Interpreter','LaTex');
end

function lookupDomain = GenerateDomain( xRange, yRange, zRange, refinementCount, scales )
%GENERATEELLIPSOIDALDOMAIN Generates a domain with one or more isocahedrals
% each refined refinementCount times
%#ok<*DTRIREP>
% ^ removes matlab messages about upcoming TriRep deprecation

% Generate icosahedral mesh of nodeCount nodes
baseTr=IcosahedronMesh; 
baseTr=SubdivideSphericalMesh(baseTr,refinementCount); 

% Set centres
xCentre             = mean(xRange);
yCentre             = mean(yRange);
zCentre             = mean(zRange);
shapeCentre         = [xCentre yCentre zCentre];

% Set semi axis lengths
saLength            = @(range) (range(2) - range(1)) / 2;
xLength             = saLength(xRange);
yLength             = saLength(yRange);
zLength             = saLength(zRange);
shapelength         = [xLength yLength zLength];

% Created duplicates of various radii
originBall           = scaleTri(baseTr, shapelength);
for scale = scales
    newBall             = scaleTri(originBall, 2^(scale));
    originBall          = ConcatTri(originBall, newBall);
end

% Add shifts
concatBall = translateTri(originBall, shapeCentre);

% Record coordinate tuples
lookupDomain.TR     = concatBall;
lookupDomain.tuples = unique(concatBall.X,'rows');

% Perturb columns with gaussian noise
% for perturbPass = 1
%     for dimension = 2:3
%         dimMean = shapeCentre(dimension);
%         dimVar = shapelength(dimension);
%         noiseyColumn = normrnd(dimMean, dimVar, size(lookupDomain.tuples, 1), 1);
%         lookupDomain.tuples(:,dimension) = lookupDomain.tuples(:,dimension) + noiseyColumn;
%     end
% end

% Add point at centre
lookupDomain.tuples = [lookupDomain.tuples; shapeCentre];

% Record centre
lookupDomain.centre = shapeCentre;

% Build a KD-tree
lookupDomain.tree = KDTreeSearcher(lookupDomain.tuples);
end

function concatTR = ConcatTri(triA, triB)
concatTR = TriRep([triA.Triangulation;
                    triB.Triangulation + max(max(triA.Triangulation))],...
                  [triA.X(:,1); triB.X(:,1);], ...
                  [triA.X(:,2); triB.X(:,2)], ...
                  [triA.X(:,3); triB.X(:,3)]);
end

function translatedTR = translateTri(tri, translationVec)
M = makehgtform('translate',translationVec);
translatedPts = (M*[tri.X ones(size(tri.X,1),1)]')';
translatedTR = TriRep(tri.Triangulation, translatedPts(:,1), ...
                                       translatedPts(:,2), ...
                                       translatedPts(:,3));
end

function scaledTR = scaleTri(tri, scaleVec)
M = makehgtform('scale',scaleVec);
scaledPts = (M*[tri.X ones(size(tri.X,1),1)]')';
scaledTR = TriRep(tri.Triangulation, scaledPts(:,1), ...
                                       scaledPts(:,2), ...
                                       scaledPts(:,3));
end