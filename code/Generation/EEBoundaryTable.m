% Script-like function
function [lookupDomain, lookupTable] = EEBoundaryTable(inputData)

% Check we are using a 2D domain
if (sum(~inputData.constantDims) ~= 2)
    error('Must be a 2D input');
end

% Generate the lookup domain
lookupDomain = GenerateDomain( inputData );
lookupTable = GenerateTable( inputData, lookupDomain );
end

function lookupDomain = GenerateDomain( inputData )

% Tolerance for secant method, and distance from EE to place points
TOL = 1e-6;
eps = 1e-2;
stradlleCount = 0;

% Get constant sigSqrT and deltaT
constSigSqrT = inputData.sigSqrT(1);
constDeltaT = inputData.deltaT(1);

% Preallocate space for 2 tuples per rT element
concatTuples    = zeros((stradlleCount+1)*size(inputData.rT,1), 2);
tuplesAdded     = 1;

% Set up base opt struct (-1 to indicate not yet determined)
a = -1; b = -1; c = constSigSqrT; d = constDeltaT;
opt = struct('S', a, 'K', 1, 'r', b/c, 'sig', 1, 'tau', c, 'delta', d/c);

% For each rT element
for rT = linspace(min(inputData.rT), max(inputData.rT), inputData.raw.dimSizes(2));
    % -- Find early exercise boundary with secant method --
    
    % Fill r field
    opt.r = rT/c;
    
    % Initial guesses
    S(1)        = max(inputData.SinvK);
    opt.S       = S(1);
    price(1)    = LRBinomial(opt);
    
    S(2)        = S(1) - 0.01;
    opt.S       = S(2);
    price(2)    = LRBinomial(opt);
    n           = 2;
    
    % Find root in SinvK direction
    while abs(price(n) - (1 - S(n))) > TOL
        % Update root guess
        n = n + 1;
        S(n) = S(n-1) + price(n-1)*(S(n-1) - S(n-2)) ...
                                  /(price(n-1) - price(n-2));
        opt.S       = S(n);
        
        % Re-evaluate price
        price(n)    = LRBinomial(opt);
    end
    
    % Add tuples
    concatTuples(tuplesAdded,:) = [S(n), rT];
    tuplesAdded = tuplesAdded + stradlleCount + 1;
end

% Record individual dimensions
lookupDomain.SinvK      = concatTuples(:,1);
lookupDomain.rT         = concatTuples(:,2);
lookupDomain.sigSqrT    = inputData.sigSqrT';
lookupDomain.deltaT     = inputData.deltaT';

% Record all tuples
lookupDomain.tuples     = unique(concatTuples,'rows');

end

function translated2D = translate2D(points, translationVec)
M = makehgtform('translate',[translationVec 0]); % expect 2D input
pointCount = size(points,1);
translated2D = (M*[points zeros(pointCount, 1) ones(pointCount, 1)]')';
translated2D = translated2D(:,1:2);
end

function scaled2D = scale2D(points, scaleVec)
M = makehgtform('scale',[scaleVec 1]);
pointCount = size(points,1);
scaled2D = (M*[points zeros(pointCount, 1) ones(pointCount, 1)]')';
scaled2D = scaled2D(:,1:2);
end