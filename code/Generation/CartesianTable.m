% Script-like function
function [lookupDomain, lookupTable] = CartesianTable( inputData )

% Check we are using a 2D domain
if (sum(~inputData.constantDims) ~= 2 && sum(~inputData.constantDims) ~= 3)
    error('Must be a 2D or 3D input');
end

if (strcmp(inputData.type, 'JR'))
    xRange = inputData.SinvK;   xLabel = '$\frac{S}{K}$';
    yRange = inputData.rT;      yLabel = '$rT$';
    if (sum(~inputData.constantDims) == 3)
        zRange = inputData.sigmaSqrT;   zLabel = '$\sigma^2 T$';
    end
elseif (strcmp(inputData.type, 'PartJR'))
    xRange = inputData.SinvK;   xLabel = '$\frac{S}{K}$';
    yRange = inputData.T;       yLabel = '$T$';
    if (sum(~inputData.constantDims) == 3)
        zRange = inputData.sigma;       zLabel = '$\sigma$';
    end
else
    error('Not yet implemented');
end

% Generate the shape
if (sum(~inputData.constantDims) == 2)
    lookupDomain = GenerateDomain( inputData, xRange, yRange );
elseif (sum(~inputData.constantDims) == 3)
    lookupDomain = GenerateDomain( inputData, xRange, yRange, zRange );
end
lookupTable = GenerateTable( inputData, lookupDomain );

% Illustrate shape
figure('color','w'); hold on;
scatter(lookupDomain.tuples(:,1), lookupDomain.tuples(:,2));
xlabel(xLabel,'Interpreter','LaTex');
ylabel(yLabel,'Interpreter','LaTex');
if (sum(~inputData.constantDims) == 3)
    zlabel(zLabel, 'Interpreter','LaTex');
end

end

function lookupDomain = GenerateDomain( inputData, xRange, yRange, zRange )
% Generate equally spaces grid
xGrid = linspace(min(xRange), max(xRange), inputData.dimSizes(1));
yGrid = linspace(min(yRange), max(yRange), inputData.dimSizes(2));
zExists = false;
if (exist('zRange', 'var'))
    zGrid = linspace(min(zRange), max(zRange), inputData.dimSizes(3));
    zExists = true;
end

% Take Cartesian Product
if zExists
    lookupDomain.tuples     = allcomb(xGrid, yGrid, zGrid);
else
    lookupDomain.tuples     = allcomb(xGrid, yGrid);
end

% Record individual dimensions
lookupDomain.xGrid = xGrid;
lookupDomain.yGrid = yGrid;
if zExists
    lookupDomain.zGrid = zGrid;
end

% Build a KD-tree
lookupDomain.tree = KDTreeSearcher(lookupDomain.tuples);

end
