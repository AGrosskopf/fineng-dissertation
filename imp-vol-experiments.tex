\section{Implied Volatility}
\label{sec:imp-vol}
 Most option pricing models require advanced knowledge of at least six parameters $S, K, T, r, \sigma, \delta$. Of these the information about the spot price $S$, strike price $K$, and expiration time $T$ will be readily available from the options exchange one is interacting with. Determining the risk-free rate $r$ and dividend rate $\delta$ takes little more effort than a Google search. Certainly the most difficult parameters to determine is the market volatility $\sigma$. One approach is to assume that the market has already priced in a volatility into its valuation of each option, and therefore we can discover this ``implied volatility'' by finding the volatility such that our model gives the same prices as the market, with all other parameters held constant. Formally we seek the value of $\sigma_{\text{imp}}$ such that $\hat{P}(S, K, T, r, \sigma_{\text{imp}}, \delta) = P$ where $P$ is the price observed in the market and $\hat{P}(S, K, T, r, \sigma_{\text{imp}}, \delta)$ is the price given the option pricing model in question.

The above problem can be reformulated as a root-finding problem for $f(\sigma)$ where $f(\sigma) \:= \hat{P}(S, K, T, r, \sigma, \delta) - P$ and so we can apply a root finding method such as the secant method. An optimized variation on the secant method is implemented in Matlab via the \verb|fzero| function. With a context-independent option pricing model $\hat{P}$ such as \cite{Li2010} or the even the LR binomial method fixing the parameters other than sigma can be achieved with the trivial use of anonymous functions (sometimes known as lambdas). However as the RBF method is context sensitive we must fix the other aspects of the environment such as the lookup domain and table. This is possible in Matlab with nested functions but special attention must be devoted to the implementation so as to ensure it has no detrimental effect on the performance of the interpolation.

\subsection{Results}
In the results below we seek to find the implied volatility given real market data on AAPL American puts. The data set for these tests was obtained from the Chicago Board of Options Exchange. The parameters for this option were as follows

\begin{align*}
	S_0 = 105.92 &\quad r = 0.019173 \\
	T = 0.833021 &\quad \delta = 0.0196
\end{align*}

The $(K, P)$ pairs and more information about the data set is given in appendix \ref{app:imp-vol-data}.

The shape of the lookup domain we assume is the Fibonacci spiral embedded in the partial Joubert and Rogers 5D subspace, as described \ref{sec:2dresults}. The only modifications made are to replace the variable parameter $T$ replaced with $\sigma$ and to extend the $\frac{S}{K}$ range so as to adequately cover the range in present in the data. The volatility curve implied by this data under the LR Binomial, RBF MQ and RBF TPS methods are illustrated in \ref{fig:impvol-AAPL}. The times taken by each method to compute the volatility curve are given in table \ref{tbl:imp-vol-times}. 

\begin{table}[h!]
\centering
\begin{tabular}{llll}
                  & \textbf{LR Binomial} & \textbf{RBF MQ} & \textbf{RBF TPS} \\
\textbf{Time (S)} & 12.247807            & 1.447931        & 1.080352        
\end{tabular}
\caption{Time taken to compute volatility curve.}
\label{tbl:imp-vol-times}
\end{table}

\clearpage

\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.8]{images/imp-vol-AAPL.png}
	\caption{The implied volatility of AAPL given by LR Binomial, RBF MQ and RBF TPS}
	\label{fig:impvol-AAPL}
\end{figure}

The RBF MQ method matches the shape of the volatility curve better than the TPS method over the full range of data, and the trademark ``volatility smile'' shape is clearly present. However when the strike and spot prices are close the TPS method is more accurate. The TPS method is also slightly faster than the MQ method. The absolute errors of the RBF curves with respect to the LR Binomial curve is illustrated in figure \ref{fig:impvol-AAPL-abs}.

\begin{figure}[h!]
	\centering
		\includegraphics[scale=0.8]{images/imp-vol-diff-AAPL.png}
	\caption{The absolute errors of the implied volatility surface given by RBF MQ and RBF TPS}
	\label{fig:impvol-AAPL-abs}
\end{figure}