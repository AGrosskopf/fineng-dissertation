\chapter{Table Construction}
\label{cha:lookup-table}
In this chapter we discuss in some detail the process of generating the lookup table. This can be decomposed into the two subproblems of choosing an appropriate lookup domain and of mapping that domain to the associated prices in an efficient way. In each step of this process we will examine choices and deliberate potential optimizations that can impact the performance of any subsequent interpolation. 

\section{Choosing a subspace}\label{sub-sec:choose-subspace}
The lookup table can be thought of as function $p$ with a finite domain $\tilde{D}$ and range $R$. We obtain $\tilde{D}$  by sampling a subspace $D \subset \mathbb{R^N}$ in some way. The problem of determining the subspace $D$ is surprisingly tricky yet remarkably influential on the performance of the interpolation. There are some general aims we want to achieve in our choice of $D$.

\begin{itemize}
\item All realistic scenarios should be covered by corresponding points in D.
\item No point in D should correspond to an impossible or unrealistic scenario.
\item Points in D should be spaced sparsely in regions where the pricing function changes slowly, and placed densely where the pricing function changes rapidly.
\end{itemize}

\subsection{Trivial Subspace (6D)}
The trivial choice for $D$ is the span of all 6 parameters involved in the American option pricing function: $S_0, K, T, r, \sigma, \delta$. For each parameter we define a sensible range of realistic values, and take D to be the cross product of these 6 ranges. 

\begin{equation*}
\begin{split}
D &= \lbrack S_0^{\text{min}}, S_0^{\text{max}} \rbrack \times \lbrack K^{\text{min}}, K^{\text{max}} \rbrack  \times \lbrack T^{\text{min}}, T^{\text{max}} \rbrack  \\
&\times \lbrack r^{\text{min}}, r^{\text{max}} \rbrack  \times \lbrack \sigma^{\text{min}}, \sigma^{\text{max}} \rbrack  \times \lbrack \delta^{\text{min}}, \delta^{\text{max}} \rbrack \\
\end{split}
\end{equation*}

While this approach is conceptually simple, it has a number of significant disadvantages. The most important disadvantage is that in order to discretize each of the 6 dimensions we will need an exceedingly large number of points. For example, in order to place 10 nodes along each dimension, we will have $10^6$ nodes. If we can reduce the number of dimensions of the pricing function, we can reduce the number of nodes in the discretized domain. This will in turn reduce the time to compute the lookup table and the space it takes to store; however, this is a secondary benefit as we only compute the lookup table once. The primary advantage of reducing the number of dimensions is found in the interpolation phase. This is because the computational complexity of the process of choosing points from the lookup table grows with the number of dimensions.
         
\subsection{Joubert and Rogers Subspace (4D)}\label{sub-sub-sec:JR-subspace-4d}
Joubert and Rogers (1999) took an analytical approach to reducing the
 dimensionality of the problem, giving the following relationship between the put value $P$ and the lookup table $p$

\begin{align}\label{eq:jr-4d-reduction}
	P(S_0, K, T, r, \sigma, \delta) = Kp(\frac{S_0}{K}, rT, \sigma^2 T, \delta T)
\end{align}
	
Thus following a trivial transformation, we can take $D$ to be the following 4-dimensional subspace

\begin{equation*}
\begin{split}
D &= \lbrack \frac{S_0^{\text{min}}}{K}, \frac{S_0^{\text{max}}}{K} 				\rbrack \times 
	 \lbrack r^{\text{min}}T^{\text{min}}, r^{\text{max}}T^{\text{max}} 			\rbrack \\
&\times 
	 \lbrack (\sigma^{\text{min}})^2 T^{\text{min}}, 								(\sigma^{\text{max}})^2 T^{\text{max}} \rbrack  \times 
	 \lbrack \delta^{\text{min}}T^{\text{min}}, \delta^{\text{max}}					T^{\text{max}} \rbrack \\
&:= \lbrack d_1^{\text{min}}, d_1^{\text{max}} \rbrack \times 
    \lbrack d_2^{\text{min}}, d_2^{\text{max}} \rbrack \times 
    \lbrack d_3^{\text{min}}, d_3^{\text{max}} \rbrack \times 
    \lbrack d_4^{\text{min}}, d_4^{\text{max}} \rbrack \\
\end{split}
\end{equation*}

\begin{remark}[Numerical stability of 4D reduction]
A notable feature of this reduction is that moving along any one of three time-related dimensions $ \{ d_2, d_3, d_4 \} $, with the remaining dimensions held constants, does not imply a change in exactly one variable in the original six-dimensional problem. For example a change in $rT$ could be caused by either a change in $r$ or a change in $T$ or both. This has an important numerical effect on the stability of the system in that a small change of the non-dimensional variable can be caused by much larger changes in both of the dimensional variables. This effect is particularly pronounced close to expiry ($T < 0.1$) and all along the $\sigma^2T$ dimension, although these sets of parameters were not considered in the numerical experiments conducted by Joubert and Rogers.
\end{remark}

\subsection{Partial Joubert and Rogers Subspace (5D)}\label{sub-sub-sec:JR-subspace-5d}

In an effort to eliminate some of the numerical instability present in the reduced form \eqref{eq:jr-4d-reduction} the author also considered a partially reduced form given by

\begin{align}\label{eq:jr-5d-reduction}
	P(S_0, K, T, r, \sigma, \delta) = Kp(\frac{S_0}{K}, T, r, \sigma, \delta)
\end{align}

As we shall see later on in the paper this compared to the original formulation, this formulation allows for far superior accuracy and stability. The corresponding 5-dimensional subspace is

\begin{equation*}
\begin{split}
D &= \lbrack \frac{S_0^{\text{min}}}{K}, \frac{S_0^{\text{max}}}{K} 				\rbrack \times 
	 \lbrack r^{\text{min}}, r^{\text{max}} 				  \rbrack  \times 
	 \lbrack \sigma^{\text{min}}, \sigma^{\text{max}} \rbrack  \times 
	 \lbrack \delta^{\text{min}}, \delta^{\text{max}}	\rbrack \times 
	 \lbrack T^{\text{min}}, T^{\text{max}}	\rbrack \\
&:= \lbrack \xi_1^{\text{min}}, \xi_1^{\text{max}} \rbrack \times 
    \lbrack \xi_2^{\text{min}}, \xi_2^{\text{max}} \rbrack \times 
    \lbrack \xi_3^{\text{min}}, \xi_3^{\text{max}} \rbrack \times 
    \lbrack \xi_4^{\text{min}}, \xi_4^{\text{max}} \rbrack \times
    \lbrack \xi_5^{\text{min}}, \xi_5^{\text{max}} \rbrack \\
\end{split}
\end{equation*}

\section{Discretizing the Domain} \label{sub-sec:discrete-domain}

We are required to discretize $D$ in some sensible way to obtain a finite set of points $\tilde{D}$. Perhaps the most notable advantage of RBF is that they belong to a class of interpolation methods known as \textbf{meshfree methods}. This means they are not bound to operate on a Cartesian grid, and as we shall explore later on in the paper, superior results can be obtained by choosing a distribution of points more aligned to the problem. For the purposes of this chapter we will discuss the modifications Joubert and Rogers to the standard Cartesian grid.

\subsection{Discrete Joubert and Rogers (4D)}\label{sub-sub-sec:discrete-JR}
The easiest way to sample the 4-D subspace given in \ref{sub-sub-sec:JR-subspace-4d} is by employing an equidistant grid in which nodes are placed in fixed intervals along each dimension. Under this scheme we first decide how many nodes we want along each dimension, say $n_1, n_2, n_3, n_4$. We then compute a dimensional discretization of each of the 4 dimensions: 

\begin{align*}
\tilde{D_j} & =\left\lbrace d_j^{\text{min}} + ih_j \colon i = 0, 1, ..., n_j - 1 \right\rbrace & \text{where } h_j = \frac{d_j^{\text{max}} - d_j^{\text{min}}}{n_j - 1} \\
&&\text{for } j = 1, 2, 3, 4
\end{align*}

The complete discretized domain is given by the Cartesian product of the individual discretized dimensions $\tilde{D} = \prod_{j=1}^{4} \tilde{D_j}$. A similar approach can be used for the 5-D subspace given in \eqref{eq:jr-5d-reduction}.

However there are a number of ways to improve upon this by placing nodes more densely in regions in which the pricing function is assumed to change rapidly. One such region is close to expiry, I.E. where $T$ approaches 0. This might be achieved by redefining $h_2, h_3$ and $h_4$ to be logarithmic functions of their respective dimensions $h_2(rT), h_3(\sigma^2 T)$ and $h_4(\delta T)$ respectively. Joubert and Rogers (1999) experimented with such a configuration and report that 'the slight improvement in precision did not seem to justify the additional complication in accessing the table and interpolating the option prices'.

One modification Joubert and Rogers (1999) did recommend is to dynamically place the nodes along $\tilde{D_1}$ according to where the option price is significantly greater than zero. To achieve this we consider each tuple $(rT, \sigma^2 T, \delta T) \in \prod_{j=2}^{4} \tilde{D_j}$ in turn, and set $d_1^{\text{max}}$ to be the smallest value for which the option price is significantly bigger than a given tolerance \verb|TOL| $ \ll 1$. Thus as $T \rightarrow 0$ we have $d_1^{\text{max}} \rightarrow K$ and so the indirect effect of this constraint is the the nodes along $\tilde{D_1}$ become more and more dense as $T \rightarrow 0$. This helps compensate for the fact that the option pricing function has larger changes in $\tilde{D_1}$ as the option approaches expiry. This introduces an extra computational task of order $O(n_2 n_3 n_4)$ to the generation of the lookup domain.

A further way to optimise the lookup domain is to prune tuples which correspond to scenarios inconsistent with our assumptions about individual parameters. For instance
\begin{align*}
dT =: x \in \tilde{D_4}, \delta \leq \delta^\text{max} &\Rightarrow T \geq \frac{x}{\delta^\text{max}} \\
r \geq r^\text{min} &\Rightarrow rT \geq r^\text{min}\frac{x}{\delta^\text{max}}
\end{align*}

Consider $x = d_4^{\text{min}} = \delta^{\text{min}}T^{\text{min}} \in \tilde{D_4}$. We have 

\begin{align*}
r^\text{min}\frac{x}{\delta^\text{max}} = r^\text{min}\frac{\delta^{\text{min}}}{\delta^\text{max}}T^{\text{min}} < r^\text{min}T^{\text{min}}
\end{align*}

This illustrates how taking the Cartesian product of the individual dimensions $\tilde{D_j}$ can create inconsistent points. Hence each point in each of $\tilde{D_2}, \tilde{D_3}, \tilde{D_4}$ gives rise to constraints on the other two dimensions. We identify those points which violate those constraints and prune them from the lookup domain. The reduction in size of the lookup domain will depend on the choice of parameters mentioned above. In the authors experience the reduction in the size of the table was around 10\%.

\section{Mapping the Table}

Once the discretized domain $\tilde{D}$ is determined we proceed to compute the lookup table $p$. This can be done with any sufficiently-accurate method and the author has chosen to employ the Leisen-Reimer binomial method with 5000 steps. 

The data structure underlying $p$ can have a major impact on lookup times. For example suppose we want to use $k$ points for a local interpolation of the point $\bm{x} \in \mathbb{R}^N$. If the domain is a straightforward Cartesian domain then we can further assume $k = p^N$ and we want to choose the $p$ closest points to $x$ in each dimension. With the non-equidistant modifications suggested in \ref{sub-sub-sec:discrete-JR} this is more involved but still feasible. However with a non-Cartesian domain, in particular for scattered data, this approach wont work. Instead we must find the $k$ \textbf{nearest neighbors} to $\bm{x}$.

If we structure the lookup domain as a simple matrix where each row corresponds to a tuple in $\tilde{D_j}$, we would have to consider each tuple in turn so as to work out its distance to $\bm{x}$. Thus the complexity of this procedure is $O(M)$. A more appropriate structure is a k-d tree, in which the domain is recursively partitioned into multiple sub-domains which are pairwise disjoint but who's union reforms the original domain. In the construction of a k-d tree the tuples are categorised by which subdomain they belong to. When finding the nearest neighbours of $\bm{x}$, the search is greatly aided by considering only those tuples in $\tilde{D_j}$ belonging to the same or a neighboring subdomain of $\bm{x}$. The computational complexity of searching a k-d tree is on average $O(\log{M})$ - a significant improvement on the aforementioned approach. The construction of the k-d tree can be done at the same time as constructing the lookup table.

\begin{figure}[h]
	\centering
		\includegraphics[scale=0.7]{images/knn.png}
  \vspace{-10pt}
	\caption{Illustration of kNN point selection in $\mathbb{R}^2$ for $k = 6$.}
	\label{fig:knn}
\end{figure}
