\chapter{Radial Basis Functions}
\label{cha:rbf}
In this chapter we introduce the principal theoretical concepts surrounding radial basis functions and their uses in the context of interpolation. We identify some common classes of radial basis functions. We also explore their convergence and uniqueness properties by contrast to polynomial interpolation. Finally we briefly discuss some of the details involved in computing the interpolant and suggest some ways of resolving them.

To start let us consider the interpolation problem in a general form. We have a function $f : \mathbb{R}^N \rightarrow \mathbb{R}$ and we are given that the value of $f$ at a certain set of points $X = \lbrace \bm{x_0}, \bm{x_1},... , \bm{x_M} \rbrace$ is $Y = \lbrace y_0, y_1,..., y_M \rbrace$ such that $\forall i = 0, 1,..., M, f(\bm{x_i}) = y_i$. We wish to know the value of $f$ for $\bm{x} \in \mathbb{R}^N \setminus X$. However we cannot simply evaluate $f$ at $\bm{x}$ since, $f$ may be  unavailable analytically or, as in our use case, is too computationally expensive to compute. Thus we wish to find a function $b : \mathbb{R}^N \rightarrow \mathbb{R}$ which approximates $f$ within the subspace enclosed by $X$.

As a point of reference we will first consider \textbf{polynomial interpolation} and some basic theory surrounding it.

\begin{definition}[Polynomial Interpolation]
In the case polynomial interpolation $b$ has the form for $N=1$ 

\begin{align}\label{eq:poly-interp}
b(x) = \sum_{i=0}^{M} c_i p_i(x)
\end{align}

where $\left\{ p_i(x) \right\}_{i=0}^{M}$ forms a basis of the uni-variate polynomial space $\mathcal{P}_M(\mathbb{R})$ and $c_i$ are constants which are to be determined. We also impose the additional constraints

\begin{align}\label{eq:poly-constraint}
b(x_i) = f(x_i) = y_i \, \forall i = 0, 1, ..., M
\end{align}
\end{definition}

Some examples of polynomial basis functions are given in table \ref{tbl:poly-examples}

\begin{table}[ht!]
\centering
\begin{tabular}[c]{ m{4cm} m{5cm}  }
 \textbf{Name} & \textbf{Form}\\
 \hline
 Monomial & $ p_i(x) = x^i $ \\
 Lagrange & $ p_i(x) = \prod_{x_j \in X}\frac{x - x_j}{x_i - x_j} $ \\
 Legendre & $ p_i(x) = \prod_{j = 0}^{i - 1}(x -  x_j)$ \\
 \hline
\end{tabular}
\caption{Baseis of $P_M$}
\label{tbl:poly-examples}
\end{table}

We observe that we can summarize \eqref{eq:poly-interp} and \eqref{eq:poly-constraint} in matrix form.

\begin{align}\label{eq:poly-matrix}
A\bm{c} = \bm{b}
\end{align}

Where $A$ is an $M$ by $M$ matrix with entries $a_{i,j} = p_i(x_j)$, $\bm{c}$ is an $M$ by $1$ column vector with entries $\bm{c}_i = c_i$ and $\bm{b}$ is also an $M$ by $1$ column vector with entries $\bm{b}_i = y_i$. The objective of polynomial interpolation is to find $\bm{c}$.

Before we discuss under what circumstances this problem can be solved let us first recap some basic terminology.

\begin{definition}[Well-posed Problem]
A well posed problem was defined by \cite{Hadamard1902} to be one which has the following three properties
\begin{itemize}
	\item A solution exists.
	\item That solution is unique.
	\item The solution depends 'continuously' on the input data.
\end{itemize}
\end{definition}

Conversely if a problem is not well posed it is said to be \textbf{ill-posed}. A problem may be ill-posed because it admits more than one solution, or because no solution exists. A trivial example of a problem for which there are multiple solutions is 'Find $x \in \mathbb{R}$ such that $x^2 = 2$' since $x = \sqrt{2}$ and $x = -\sqrt{2}$ are both solutions. Similarly the same problem admits no solutions in $\mathbb{N}$.

In the polynomial case the problem is well-posed if there is exactly one $q(x)$ satisfying \eqref{eq:poly-interp} and \eqref{eq:poly-constraint}. It turns out that there is and the proof requires no more than elementary linear algebra.

\begin{proposition}[Existence and Uniqueness of the Polynomial Interpolant]
For $M + 1$ distinct data points $x_i$ and function values $y_i$ the column-vector $\bm{c}$ in \eqref{eq:poly-matrix} exists and is uniquely determined.
\begin{proof}
We pre-multiply both sides of \eqref{eq:poly-matrix} by $A^{-1}$ to obtain
	\[
			\bm{c} = A^{-1}\bm{b}
	\]

Thus $\bm{c}$ exists and is unique iff $A^{-1}$ exists and unique iff $\det(A) \neq 0$. It can be shown that $A$ is a Vandermonde matrix thus its determinant is given by
	\[
			\det(A) = \prod_{i,j=0, i<j}^{M}(x_i - x_j)
	\]
Since the data points are distinct $x_i - x_j \neq 0 \forall i < j$. Hence $\det(A) \neq 0$.
\end{proof}
\end{proposition}

\begin{remark}[Invertibility of $A$]
The key takeaway from this proof is that the existence and uniqueness of the problem is equivalent to that of the matrix $A^{-1}$. When $A$ is \textbf{singular} - I.E. it has no unique inverse - the problem is ill-posed, and vice-versa. Indeed this relationship also holds true for interpolation schemes other than polynomial interpolant, including radial basis function interpolation.
\end{remark}

\clearpage

\begin{remark}[Relevance of basis]
In the polynomial case the invertibility of $A$ is independent of the choice of basis $\left\{ p_i(x) \right\}_{i=0}^{M}$. This doesn't hold true for radial basis interpolation as we will soon see. That said the computational cost of computing the inverse of a matrix is related to its structure. For example if Newtons basis is chosen, then $A$ will have a lower-triangular structure. However by choosing the monomial basis we obtain a dense matrix $A$.  From a computational perspective Newtons basis may be preferable since inverting a lower-triangular matrix is less costly than inverting a dense matrix.
\end{remark}

Now we move on to \textbf{radial basis function interpolation} and contrast it with polynomial interpolation.

\begin{definition}[RBF interpolation]
In the case radial basis function (RBF) interpolation the interpolant $b$ of the function $f$ has the form

\begin{align}\label{eq:rbf-interp}
b(\bm{x}) = \sum_{i=0}^{M} c_i \phi(\bm{\norm{\bm{x} - \bm{x_i}}})
\end{align}

where $\bm{c_i}$ are constant-vectors which are to be determined and $\phi(\bm{\norm{\bm{x} - \bm{x_i}}})$ is the known radial basis function. As is common, unless otherwise specified, we assume $\norm{\cdot}$ to be the Euclidean norm. We also impose the additional constraints

\begin{align}\label{eq:rbf-constraint}
b(\bm{x_i}) = f(\bm{x_i}) = y_i \, \forall i = 0, 1,..., M
\end{align}
\end{definition}

Some examples of radial basis functions are given in table \ref{tbl:rbf-examples}

\begin{table}[ht!]
\centering
\begin{tabular}[c]{ m{4cm} m{5cm}  }
 \textbf{Name} & \textbf{Form}\\
 \hline
 Linear & $ \phi(r) = r $ \\
 Cubic & $ \phi(r) = r^3 $ \\
 Thin-Plate Spline & $ \phi(r) = r^2\log(r)$ \\
 Multiquadric & $ \phi(r) = \sqrt{\epsilon^2 + r^2} $ \\
 Inverse Multiquadric & $ \phi(r) = \frac{1}{\sqrt{\epsilon^2 + r^2}} $ \\
 Inverse Quadric & $ \phi(r) = \frac{1}{\epsilon^2 + r^2} $ \\
 \hline
\end{tabular}
\caption{Common Radial Basis Functions}
\label{tbl:rbf-examples}
\end{table}

Again \eqref{eq:rbf-interp} and \eqref{eq:rbf-constraint} can be expressed in matrix form as

\begin{align}\label{eq:rbf-matrix}
A\bm{c} = \bm{b}
\end{align}

Where $A$ is an $M$ by $M$ matrix with entries $a_{i,j} = \phi(\bm{\norm{\bm{x_i} - \bm{x_j}}})$, $\bm{c}$ is an $M$ by $1$ column vector with entries $c_i$ and $\bm{b}$ is also an $M$ by $1$ column vector with entries $\bm{b}_i = y_i$. The objective of RBF interpolation is to find $\bm{c}$.

Unlike the polynomial case, the invertibility of $A$ is not straight-forward. As in the polynomial case we required the input data $X$ to be distinct points. However unlike the polynomial case, the choice of basis $\phi(r)$ also determines whether $A$ is singular. Henceforth we assume $X$ is not a multi-set and so the input data consists of distinct points.

\cite{Schoenberg1938} gives an important result which relates to the invertibility of $A$ given some conditions on $\phi(r)$. From this result it can be shown that the Inverse Quadric and Inverse Multiquadric RBFs defined in table \ref{tbl:rbf-examples} are well-posed, among others. More relaxed conditions were provided by \cite{Micchelli1984} which extended the list to include the Multiquadric and linear RBFs.  A notable omission from the list was the Thin-Plate Spline (TPS) RBFs as it didn't meet the criteria for Micchellis proof. A second theorem showed that TPS is well-posed for under a modified setup known as \textbf{augmented RBF interpolation}.

\begin{definition}[Augmented RBF interpolation]
In the case radial basis function (RBF) interpolation the interpolant $b$ of the function $f$ has the form

\begin{align}\label{eq:aug-rbf-interp}
b(\bm{x}) = \sum_{i=0}^{M} c_i \phi(\bm{\norm{\bm{x} - \bm{x_i}}}) + \sum_{j=0}^K d_i p_i(\bm{x})
\end{align}

where as before $\bm{c_i}$ are constant-vectors which are to be determined and $\phi(\bm{\norm{\bm{x} - \bm{x_i}}})$ is the known radial basis function. The additional summation has the form of a typical polynomial interpolation where $\left\{ p_i(x) \right\}_{j=0}^{K}$ forms a basis of the multi-variate polynomial space $\mathcal{P}_K(\mathbb{R}^N)$ and $K = \frac{N(N-1)}{2}$.

We also impose a modified set of constraints

\begin{align}\label{eq:aug-rbf-constraint}
\sum_{i=0}^M c_i p_i(\bm{x}) &= 0, \\
b(\bm{x_i}) = f(\bm{x_i}) &= y_i \, \forall i = 0, 1,..., M
\end{align}
\end{definition}

The matrix form of \eqref{eq:aug-rbf-interp} and \eqref{eq:aug-rbf-constraint} for $N=2$ is

\begin{equation}\label{eq:aug-rbf-interp-matrix}
\mleft[
\begin{array}{c c c c|c}
  a_{0,0} & a_{0,1} & \cdots & a_{0,M} & 1 \\
  a_{1,0} & a_{1,1} & \cdots & a_{1,M} & 1 \\
  \vdots  & \vdots  & \ddots & \vdots  & \vdots \\
  a_{M,0} & a_{M,1} & \cdots & a_{M,M} & 1 \\
	\hline
	1				& 1				& \cdots & 1 & 0
\end{array}
\mright]
\mleft[
\begin{array}{c}
  c_0 \\
  c_1 \\
	\vdots  \\
	c_M \\
	\hline
	\alpha
\end{array}
\mright]
=
\mleft[
\begin{array}{c}
  y_0 \\
  y_1 \\
	\vdots  \\
	y_M \\
	\hline
	0
\end{array}
\mright]
\end{equation}

where $a_{i,j}$ are as in \eqref{eq:rbf-matrix}.

It is worth noting that those RBFs previously shown to be invertible under the original system \eqref{eq:rbf-matrix} are also invertible under the augmented system \eqref{eq:aug-rbf-interp-matrix}. In fact it is potentially advantageous to use the latter system as the polynomial component can help improve the accuracy of the approximation.

While it is important to have this theoretical guarantee that we can invert $A$, in practice it isn't always sufficient. Matrix inversion algorithms may be unable to find the inverse of a matrix in some situations even if that inverse is known to exist. One such situation is when the matrix has a high \textbf{condition number}.

\begin{definition}[Condition Number]
The condition number of a matrix $A$, denoted $\kappa(A)$, is given by

\begin{align}
	\kappa(A) = \frac{\sigma_{\text{max}}(A)}{\sigma_{\text{min}}(A)}
\end{align}

where $\sigma_{\text{max}}(A)$ and $\sigma_{\text{min}}(A)$ are the largest and smallest singular values in the singular value decomposition of $A$.
\end{definition}

In general terms a high condition number indicates that the vector $\bm{c}$ is likely to be computed inaccurately, and conversely a low condition number indicates that we can compute it very accurately. In the context of RBF interpolation there are a number of factors which can influence the condition number \cite{Wright2003}, these include

\begin{itemize}
	\item The number of data points $M$.
	\item The value of the shape parameter $\epsilon$.
\end{itemize}

There is a trade off to be considered for both of these parameters. We may decide to choose a large number of data points $M$ to avoid or 'smooth out' bias caused by a few locally exceptional points. However the condition number of $A$ grows proportionally with its size $M$, and so under that consideration a small $M$ is preferable. Similarly a large value of the shape parameter $\epsilon$ encourages the interpolation surface to converge to that of the underlying \cite{Tong2001} but simultaneously increases the condition number of $A$. Thus when applying RBFs to the problem of interpolating American option prices, we must pay special attention to the values of $\epsilon$ and $M$ chosen. 